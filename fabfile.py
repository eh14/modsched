from fabric.api import task, local, lcd, execute
import subprocess
import time

import os
import sys
import requests
from fabric.context_managers import prefix

VIRTENV_FOLDER = 'venv'

python = os.path.join(VIRTENV_FOLDER, 'bin', 'python')
django = python + ' src/manage.py '

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))

sys.path.append(os.path.join(PROJECT_DIR, 'src'))

TEMPLATE_PATH = os.path.join(PROJECT_DIR, 'templates')

SRC_PATH_PREFIX = 'PYTHONPATH=$PYTHONPATH:src '

@task
def bootstrap():
    """
    install all python dependencies and the frontend libraries
    """
    local(python + ' setup.py develop')
    local('npm install bower')
    local('node_modules/bower/bin/bower install')
    

@task
def worker(name="localworker"):
    """
    Starts a local worker process. Working directory is settings.MODSCHED_PROJECTDIR
    """
    p = subprocess.Popen([python, 'src/manage.py', 'celeryd', '-E',
                          '--settings=modsched.workersettings',
                          '--loglevel=INFO', '-Q', 'commands', '--hostname=worker1@%h', '--time-limit=' + str(3600 * 24)])
    local(django + 'celeryd -E --settings=modsched.workersettings --loglevel=INFO -Q sidejobs --hostname=worker2@%h --time-limit=25 --soft-time-limit=10')
    p.terminate()
    p.kill()
    p.wait()
    
@task
def status():
    """
    Print status of worker and queues etc as produced by djcelery.
    """
    local(django + 'celery status -A modsched')
    local(django + 'celery -A modsched inspect active_queues')
    
@task
def truncDb():
    """
    Deletes the database to start from scratch. 
    
    Sometimes necessary to get rid of nasty migration programs after major changes. 
    All data will be lost!
    """
    local("mysql -uroot -proot -e 'drop database modsched; create database modsched;'")
    
@task
def db():
    local('mysql -uroot -proot modsched')
    
@task
def statics():
    """
    Starts the django command to collect all statics in the predefined folder.
    """
    local(django + 'collectstatic --noinput')
    
@task
def initRepo():
    local(django + 'initrepo')
    
@task
def serve(stats=False):
    """
    Start the web interface and the celery event listener.
    """
    if stats is not False:
        local(django + 'collectstatic --noinput')
        
    # execute(initRepo)
    try:
        p = subprocess.Popen([python, 'src/manage.py', 'eventlistener'])
        
        local(django + 'runserver')
    except Exception as e:
        print e
    finally:
        p.terminate()
        p.wait()
        
@task
def check(test=None):
    """
    Run unit tests
    """
    local(django + 'syncdb --settings=modsched.testsettings --noinput')
    local(django + 'initrepo --settings=modsched.testsettings')
    
    if test is not None:
        local(django + 'test --settings=modsched.testsettings ' + test)
    else:
        local(django + 'test --settings=modsched.testsettings authentication modsched.tests')
    
def _waitForUrl(url, timeout, interval=0.5):
    """
    Requests the passed url repeatedly until it becomes available.
    @param url: url to poll for checking.
    @param timeout: time after which an exception is raised.
    @param interval [optional]: interval for polling
    """
    while True:
        end = time.time() + timeout 
        try:
            requests.get(url)
            return
        except requests.exceptions.ConnectionError:
            if time.time() > end:
                raise Exception("Server url %s didn't become available in %d seconds..." % (url, timeout))
            else:
                time.sleep(interval)
            
@task
def itest(test=None):
    sys.path.append('src')
    from modsched import itests
    # remove all existing tasks in the database
    local('bin/django celery purge -f --settings=modsched.itestsettings')
    local('bin/django syncdb --settings modsched.itestsettings --noinput')
    local('bin/django loaddata users --settings modsched.itestsettings')
    try:
        server = subprocess.Popen(['bin/django', 'runserver',
                                   '--settings=modsched.itestsettings',
                                   itests.SERVER_ADDR], preexec_fn=os.setsid)
        
        # now run the camera and the worker
        cCam = subprocess.Popen(['bin/django', 'celerycam',
                                 '-v', '3',  # very verbose
                                 '--settings=modsched.itestsettings',  # use test settings (testdb)
                                 '--freq=1'  # write really often to avoid too many false positives
                                    ])
        worker = subprocess.Popen(['bin/celery', '-A', 'modsched',
                               '--loglevel=INFO', 'worker', '-E'])
            
        _waitForUrl(itests.SERVER_URL, 10)
         
        if test is not None:
            local('bin/python -m unittest --settings=modsched.itestsettings ' + test)
        else:
            local('bin/itest -w src/modsched/itests -s')
            # local('bin/django --settings=modsched.itestsettings modsched.itests')
            
    finally:
        # terminate them both
        if worker is not None:
            worker.terminate()
        if cCam is not None:
            cCam.terminate()
        if server is not None:
            os.killpg(os.getpgid(server.pid), 15)

        # wait for them to finish
        if worker is not None:
            worker.wait()
        if cCam is not None:
            cCam.wait()
        if server is not None:
            server.wait()

@task
def migrate(test=False):
    """
    create and perform database migrations.
    """
    arg = ''
    if test:
        arg = ' --settings=modsched.testsettings'
        
    local(django + 'makemigrations' + arg)
    local(django + 'migrate' + arg)
    local(django + 'createsuperuser')
    
@task
def diff():
    """
    run meld on the src dir to watch changes.
    """
    with lcd('src'):
        local('meld .')
        
        
        
#
# deployment
#
@task
def deploy(port='8008', domain='localhost', nginx_home='/etc/nginx/', virtenv_home='venv', working_directory=None):
    """
    Generate and deploy the nginx configuration. Params: port and domain
    
    @param port: port to serve applicatoin
    @param domain: domain/IP of the server
    @param nginx_home: absolute path to the nginx installation to link the configuration file to
    @param virtenv_home: relative path to the virtualenv folder of the project
    """
    
    nginx_config = 'modsched_nginx.conf'
    
    print "Generating and Linking nginx configuration....", nginx_config
    _instantiateTemplate('modsched_nginx.conf.template',
                         nginx_config,
                         hint='GENERATED FILE - ALL MODIFICATIONS WILL BE LOST',
                         port=port,
                         project_path=PROJECT_DIR,
                         domain=domain)
    
    linkedFile = os.path.join(nginx_home, 'sites-enabled', nginx_config)
    if not os.path.exists(linkedFile):
        local('sudo ln -s {source} {target}'.format(source=os.path.join(PROJECT_DIR, nginx_config),
                                                    target=linkedFile))
    else:
        print "nginx configuration already linked, skipping"

    print "generating uwsgi configuration"
    _instantiateTemplate('modsched_uwsgi.ini.template',
                         'modsched_uwsgi.ini',
                         project_path=PROJECT_DIR,
                         virtenv_home=virtenv_home)
    
    print "generating templates for hg-webview"
    if not working_directory:
        working_directory = os.path.join(PROJECT_DIR, '_workingdir_')
        print "Working directory is not specified, assuming", working_directory

    _instantiateTemplate('hgweb.config.template', 'hgweb.config', working_directory=working_directory)
    _instantiateTemplate('hgweb.wsgi.template', 'hgweb.wsgi', project_path=PROJECT_DIR)
    _instantiateTemplate('repos_uwsgi.ini.template', 'repos_uwsgi.ini', project_path=PROJECT_DIR)
    _instantiateTemplate('modsched_web.service.template', 'modsched_web.service', project_path=PROJECT_DIR, virtualenv_bin=os.path.join(PROJECT_DIR, VIRTENV_FOLDER, 'bin'))
    _instantiateTemplate('modsched_hgweb.service.template', 'modsched_hgweb.service', project_path=PROJECT_DIR, virtualenv_bin=os.path.join(PROJECT_DIR, VIRTENV_FOLDER, 'bin'))
    _instantiateTemplate('modsched_eventlistener.service.template', 'modsched_eventlistener.service', project_path=PROJECT_DIR, virtualenv_bin=os.path.join(PROJECT_DIR, VIRTENV_FOLDER, 'bin'))
    _instantiateTemplate('modsched_worker.service.template', 'modsched_commands_worker.service', project_path=PROJECT_DIR,
                         virtualenv_bin=os.path.join(PROJECT_DIR, VIRTENV_FOLDER, 'bin'),
                         queue='commands',
                         hard_timeout=(3600 * 24) + 1,
                         soft_timeout=3600 * 24)
    _instantiateTemplate('modsched_worker.service.template', 'modsched_sidejobs_worker.service', project_path=PROJECT_DIR,
                         virtualenv_bin=os.path.join(PROJECT_DIR, VIRTENV_FOLDER, 'bin'),
                         queue='sidejobs',
                         hard_timeout=60,
                         soft_timeout=59)
    
    # print "Generating production settings for django...."
    
    
    execute(statics)
        
    print "All done...."
    print "You may need to restart nginx."
    print "Start web server >uwsgi --ini modsched_uwsgi.ini"
    print "Start repo server >uwsgi --ini repos_uwsgi.ini"
    print "Server URL: http://{domain}:{port}".format(domain=domain, port=port)
    
def _instantiateTemplate(input, output, **templateValues):
    """
    Helper function that reads file <input> and copies its content to <output> formatting
    with provided templateValues.
    """
    with open(os.path.join(TEMPLATE_PATH, input), 'r') as inputFile, \
            open(output, 'w') as outputFile:
        outputFile.write(inputFile.read().format(**templateValues))

 
@task
def parseLog(parser, logFile):
    """
    Test function to run a parser using a log file to check its output. Use like fab parseLog:<parserName>,<logFile
    """
    with prefix(SRC_PATH_PREFIX):
        local('python src/modsched/itests/executeparser.py {parser} {logFile}'.format(parser=parser,
                                                                                  logFile=logFile))


@task
def lint():
    local('pylint modsched')
