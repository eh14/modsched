
from djcelery import snapshot
from celery.utils.log import get_logger
from modsched import celery_app

class ModSchedCamera(snapshot.Camera):
    """
    Listens for task events and updates the database appropriately.
    Task events on queue 'sidejobs' are usually ignored since they're only used for
    management purposes. Excdeption is the PreviewCommand-Task that is run periodically 
    on commands parsing their output for previewing. Those tasks' results are
    updated in the PreviewData model.
    """
    logger = get_logger('ModSchedCamera')
    
    def on_shutter(self, state, *args, **kwargs):
        
        for worker in state.workers.items():
            self.handle_worker(worker)
            
        for uuidTask in state.tasks.items():
            _uuid, task = uuidTask
            if task.routing_key == 'sidejobs':
                continue
                
            else:  # not parser-result, handle it normally in the database.
                self.handle_task(uuidTask)
            