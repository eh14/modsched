'''
Created on 06.03.2015

@author: mdaum
'''
import xml.sax

class PrismQuantileSaxHandler(xml.sax.ContentHandler):
    def __init__(self):
        self.model = None
        self.results = {}
        self.statistics = {}
        self.statistics['times'] = {}
        
        self.__inside_query_tag = False
        self.__current_query = None
        self.__inside_threshold_tag = False
        self.__current_content = None
        self.__threshold = None
    
    def startElement(self, name, attributes):
        self.__current_content = ''
        if name == 'threshold':
            self.__inside_threshold_tag = True
        elif name == 'query':
            self.__inside_query_tag = True
        return
    
    def characters(self, content):
        self.__current_content += content
    
    def endElement(self, name):
        #model statistics
        if name == 'model_file':
            self.model = self.__current_content
        elif name == 'construction_time':
            self.statistics['construction_time'] = float(self.__current_content)
        elif name == 'states':
            self.statistics['states'] = int(self.__current_content)
        elif name == 'transitions':
            self.statistics['transitions'] = int(self.__current_content)
        elif name == 'mtbdd':
            self.statistics['mtbdd'] = int(self.__current_content)
        elif name == 'time' and self.__inside_query_tag:
            self.statistics['times'][self.__current_query] = float(self.__current_content)
        #model checking results
        elif name == 'id' and self.__inside_query_tag:
            self.__current_query = self.__current_content
            self.results[self.__current_query] = {}
        elif name == 'value' and self.__inside_threshold_tag:
            self.__threshold = float(self.__current_content)
        elif name == 'result' and self.__inside_threshold_tag:
            self.results[self.__current_query][self.__threshold] = float(self.__current_content)
        elif name == 'threshold':
            self.__inside_threshold_tag = False
        elif name == 'query':
            self.__inside_query_tag = False
        return

def do_SAX_parsing(xml_file):
    handler = PrismQuantileSaxHandler()
    xml.sax.parseString(xml_file, handler)
    return handler.model, handler.results, handler.statistics