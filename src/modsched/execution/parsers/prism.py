'''
@author: mdaum
'''
from pyparsing import Literal, nums, ZeroOrMore, Group, Combine, Word, alphanums, oneOf, Keyword, SkipTo, CaselessLiteral, Optional, Or
import calendar
import datetime

from modsched.execution import parsers
import pyparsing

# general type definitions useful for any parser
point = Literal('.')
exponent = CaselessLiteral('E')
plus_or_minus = Literal('+') | Literal('-')
integer = Combine(Optional(plus_or_minus) + Word(nums))
two_digit_integer = Word(nums, exact=2)
number = Combine(integer + Optional(point + Optional(Word(nums))) + Optional(exponent + integer))
weekday = oneOf('Mon Tue Wed Thu Fri Sat Sun')
day = two_digit_integer
month = oneOf('Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec')
year = Word(nums, exact=4)


# general format definitions using the former types
version_format = Combine(integer + ZeroOrMore('.' + Word(alphanums)))
time_format = Combine(two_digit_integer + Literal(':') + two_digit_integer + Literal(':') + two_digit_integer)
date_format = Group(weekday.setResultsName('weekday') + month.setResultsName('month') + day.setResultsName('day') + time_format.setResultsName('starting_time') + oneOf('CET CEST').suppress() + year.setResultsName('year'))
# format to get information about the file for the model
model_file_format = Word(alphanums + '[]_,.:-/')
# format for the queries
query_format = Word(alphanums + ' []_=,<>"{}?().-+!')


# definitions used for parsing PRISM-result files
delim = Keyword('---------------------------------------------------------------------').suppress()
headline = Group(Keyword('PRISM') + Keyword('=====')).suppress()
version = Literal('Version:').suppress() + version_format
date = Literal('Date:').suppress() + date_format
host = Literal('Hostname:').suppress() + Word(alphanums + '-.')
model_file = Literal('Parsing model file "').suppress() + model_file_format + Literal('"...').suppress()
query_at_initial_queries_list = Literal('(').suppress() + integer.suppress() + Literal(')').suppress() + query_format
initial_queries_list = Group(integer.suppress() + (Literal('properties:') | Literal('property:')).suppress() + ZeroOrMore(query_at_initial_queries_list.setResultsName('id', listAllMatches=True)))
construction_time = Literal('Time for model construction:').suppress() + number + Literal('seconds.').suppress()
type_info = Literal('Type:').suppress() + oneOf('MDP DTMC CTMC')
states_info = Literal('States:').suppress() + integer
transitions_info = Literal('Transitions:').suppress() + integer
choices_info = Literal('Choices:').suppress() + integer
mtbdd_info = Literal('Transition matrix:').suppress() + integer
result_format = number ^ (Literal('[').suppress() + number + Literal(',').suppress() + number + Literal(']').suppress()) ^ Literal('Infinity')
start_query = Literal('Model checking: ').suppress() + query_format
query_time = Literal('Time for model checking:').suppress() + number + Literal('seconds.').suppress()
query_result = Literal('Result:').suppress() + result_format
regular_query_result = query_time('time') + SkipTo(query_result).suppress() + query_result('result')
query_error = Literal('Error:').suppress() + Word(alphanums + '.,!')('result')
query_outcome = Or([regular_query_result, query_error])

threshold_label = Literal('Results for threshold').suppress() + number('value') + Literal(':').suppress()
threshold_index_result_delimeter = Group(Literal(':') + Optional(Group(Literal('(').suppress() + Word(alphanums + ',').setResultsName('assignment') + Literal(')').suppress())) + Literal('='))
threshold_result = integer.suppress() + threshold_index_result_delimeter('delim').suppress() + result_format('result')
threshold_list = Group(delim.suppress() + ZeroOrMore(Group(threshold_label + threshold_result + delim.suppress()).setResultsName('threshold')))


class _PrismBaseParser(parsers.LinePreviewParser):
    MONTH_MAP = {mon:i + 1 for i, mon in enumerate(calendar.month_abbr)}
    
    # set this attribute in subclasses
    stat = None
    
    def _parse(self, inputFile, result):
        try:
            data = self.start.parseFile(inputFile)
            
            for r in  data.get('results'):
                result.addValue(float(r.get('time', [None])[0]),  # x
                                float(r.get('result', ['0'])[0]),  # y  
                                info={'id':r.get('id', [None])[0]})

        except pyparsing.ParseBaseException as e:
            raise parsers.LogParseException("Error parsing the file. Maybe incomplete (%s)" % str(e))
        except ValueError as e:
            raise parsers.LogParseException("Error interpreting some values (%s)" % str(e))
        except Exception as e:
            print e
            raise parsers.LogParseException("Something weird happend (%s)" % str(e))
            
        
class PrismParserQuantile(_PrismBaseParser):
    system_info = Group(headline + version.setResultsName('prism_version') + date.setResultsName('date') + host.setResultsName('host') + SkipTo(model_file).suppress() + model_file.setResultsName('model_file') + SkipTo(initial_queries_list).suppress() + initial_queries_list.setResultsName('queries'))
    model_info = Group(Literal('Building model...').suppress() + SkipTo(construction_time).suppress() + construction_time.setResultsName('construction_time') + SkipTo(states_info).suppress() + states_info.setResultsName('states') + SkipTo(transitions_info).suppress() + transitions_info.setResultsName('transitions') + Optional(SkipTo(mtbdd_info).suppress() + mtbdd_info.setResultsName('mtbdd')))
    model_constants = Literal('Model constants:') + Word(alphanums + '_=.,')
    model_checking = Group(start_query.setResultsName('id') + Optional(model_constants).suppress() + Optional(model_info.setResultsName('model_stats')) + SkipTo(threshold_list).suppress() + threshold_list.setResultsName('thresholds') + SkipTo(query_outcome).suppress() + query_outcome)
    start = system_info.setResultsName('meta-info') + Group(ZeroOrMore(SkipTo(delim).suppress() + delim.suppress() + model_checking.setResultsName('query', listAllMatches=True))).setResultsName('results')
    

class PrismParserV4_2_beta1(_PrismBaseParser):
    system_info = Group(headline + version.setResultsName('prism_version') + date.setResultsName('date') + host.setResultsName('host') + SkipTo(model_file).suppress() + model_file.setResultsName('model_file') + SkipTo(initial_queries_list).suppress() + initial_queries_list.setResultsName('queries'))
    model_info = Group(Literal('Building model...').suppress() + SkipTo(construction_time).suppress() + construction_time.setResultsName('construction_time') + SkipTo(states_info).suppress() + states_info.setResultsName('states') + SkipTo(transitions_info).suppress() + transitions_info.setResultsName('transitions') + Optional(SkipTo(mtbdd_info).suppress() + mtbdd_info.setResultsName('mtbdd')))
    model_checking = Group(start_query.setResultsName('id') + Optional(model_info.setResultsName('model_stats')) + SkipTo(query_outcome).suppress() + query_outcome)
    start = system_info.setResultsName('meta-info') + Group(ZeroOrMore(SkipTo(delim).suppress() + delim.suppress() + model_checking.setResultsName('query', listAllMatches=True))).setResultsName('results')
    

class PrismParserV4_0_3(_PrismBaseParser):
    system_info = Group(headline + version.setResultsName('prism_version') + date.setResultsName('date') + host.setResultsName('host') + SkipTo(model_file).suppress() + model_file.setResultsName('model_file') + SkipTo(initial_queries_list).suppress() + initial_queries_list.setResultsName('queries'))
    model_info = Group(SkipTo(construction_time).suppress() + construction_time.setResultsName('time') + SkipTo(states_info).suppress() + states_info.setResultsName('states') + SkipTo(mtbdd_info).suppress() + mtbdd_info.setResultsName('mtbdd'))
    model_checking = Group(start_query.setResultsName('id') + SkipTo(query_time).suppress() + query_time.setResultsName('time') + SkipTo(query_result).suppress() + query_result.setResultsName('result'))
    start = system_info.setResultsName('meta-info') + Group(ZeroOrMore(SkipTo(delim).suppress() + delim.suppress() + model_checking.setResultsName('query', listAllMatches=True))).setResultsName('results')

