"""

"""
import os
import random
import time

class LogParseException(Exception):
    pass


class PreviewResult(object):
    # to overwrite by subclasses
    previewType = None
    
    def __init__(self, sequence_id=0):
        self.sequence_id = sequence_id
        self.values = []
    
    def __repr__(self):
        return "Previewer result for task: sequence_id:{self.sequence_id}, data: {self.data}".format(result=self)


    def toDict(self):
        return {'sequence_id' : self.sequence_id,
                'previewType':self.previewType,
                'values':self.values}

    def addValue(self, x=None, y=None, series=None, info=None):
        raise NotImplementedError('abstract')
    

    
class LinePreviewResult(PreviewResult):
    
    previewType = 'line'
        
    def addValue(self, x, y, series=None, info=None):
        value = {'x':x,
                'y':y,
                'series':series
                }
        if info:
            value['info'] = info
            
        self.values.append(value)
   
   
class BaseParser(object):
    
    
    def parse(self, inputObject, result):
        if hasattr(inputObject, 'read'):
            self._parse(inputObject, result)
            return
            
        if not os.path.exists(inputObject):
            raise LogParseException('Log File not existing')
        with open(inputObject, 'r') as inputFile:
            self._parse(inputFile, result)
        
    def _parse(self, inputFile, result):
        raise NotImplementedError('implement in subclass!')
    
class LinePreviewParser(BaseParser):
    ParserResult = LinePreviewResult

class EchoParser(LinePreviewParser):
    """
    Simple "parser" that prints the entire contents of the file as the result.
    """
    
    def _parse(self, inputFile, result):
        for line in inputFile.readlines():
            result.addValue(0, 0, info=line)
        
class RandomParser(LinePreviewParser):
    """
    Test Parser that creates random x-y values for each call to make the impression of
    interpreting actual data.
    """
    
    
    def _parse(self, inputFile, result):
        rand = random.Random(time.time())
        
        result.addValue(int(rand.random() * 100), int(rand.random() * 100),
                        info='some more information')
