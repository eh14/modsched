from __future__ import absolute_import

from celery import shared_task
from celery.utils.log import get_task_logger
from celery.exceptions import SoftTimeLimitExceeded
import subprocess
import shlex
import time
from modsched import execution
from modsched.execution import parsers

import os
from modsched.services import store
from modsched import celery_app
import traceback
import threading
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
import json
logger = get_task_logger(__name__)

STDOUT_FILE = 'stdout.log'
STDERR_FILE = 'stderr.log'



def _getSequenceId():
    """
    Generates a sequential ID for tasks referring to the same parent to avoid 
    slow tasks overwrite faster, but later tasks.
    We use the timestamp in milliseconds because the task may be executed every 
    couple of seconds so there should be plenty of room in between.
    """
    
    return int(time.time() * 1000)



def _startPreviewerTask(cmdId, workingDir, previewerName, ignoreErrors=True):
    """
    private method to start the parser task as a subtask of this.
    """
    
    # only start if a previewer is set.
    if previewerName:
        return PreviewCommand.delay(commandExecutionId=cmdId,
                                    workingDir=workingDir,
                                    previewer=previewerName,
                                    ignoreErrors=ignoreErrors)


def _raiseProgramFailedException(errFileName, returncode):
    with open(errFileName, 'r') as errFile:
        raise Exception("""Program exit code: {rc}
STDERR: 
---------------- 
{stderr}""".format(rc=returncode, stderr=errFile.read(100)))
        
        
class OutputFilePoller(threading.Thread):
    
    def __init__(self, proc, workingDir, executionId, previewerName):
        threading.Thread.__init__(self)
        
        self.stopEvent = threading.Event()
        self.proc = proc
        self.workingDir = workingDir
        self.executionId = executionId
        self.previewerName = previewerName
        
        
    def stop(self):
        self.stopEvent.set()
        
    def run(self):
        oldSize = 0
        while self.proc.poll() is None:
            if self.stopEvent.isSet():
                return
            time.sleep(2)
            if self.stopEvent.isSet():
                return
            newSize = os.stat(self.outFileName).st_size
            if newSize != oldSize:
                oldSize = newSize
                # parsing the ParseCommand
                _startPreviewerTask(self.commandExecutionId, self.workingDir, self.previewerName)





@shared_task(bind=True,  # access to the task object 
        ignore_result=True,
        trail=False  # it may start many subtasks (parsers), don't track them )
        )
def RunCommand(self,
               experimentId,
               commandExecutionId,
               executionSequenceId,
               cmd,
               previewerName):
    """
    executes 
    """
    
        
    folder = store.getOrCreateExecutionFolder(experimentId, executionSequenceId)
    logger.debug('Executing {cmd} in {folder}'.format(cmd=cmd, folder=folder))
    outFileName = os.path.join(folder, STDOUT_FILE)
    errFileName = os.path.join(folder, STDERR_FILE)
    
    
    with open(outFileName, 'w') as outFile, open(errFileName, 'w') as errFile:
        
        p = subprocess.Popen(shlex.split(cmd),
                             stdout=outFile, stderr=errFile, bufsize=1,
                             cwd=folder)
        
        previewerThread = OutputFilePoller(p, outFileName, commandExecutionId, previewerName)
        previewerThread.start()

        # wait for the process to finish
        p.wait()
        # stop the previewer-thread and run the final previewer-task if finished
        previewerThread.stop()
        
            
    # Program failed, let the overall task fail
    if p.returncode != 0:
        _raiseProgramFailedException(errFileName, p.returncode)
    else:
        # if succeeded, do the final parse
        _startPreviewerTask(commandExecutionId, folder, previewerName, ignoreErrors=False)
        
@shared_task(bind=True,
             ignore_result=True,
             
             trail=False)
def PreviewCommand(self, commandExecutionId, workingDir, previewer, ignoreErrors=True):
    logger.debug("Starting parser in directory %s of cmd %s" % (workingDir,
                                              commandExecutionId))
    
    previewerCmds = shlex.split(previewer)
    assert previewerCmds, "empty previewer command passed, we should not have got to this point"
    
    previewerCmd = previewerCmds[0]
    
    # check whether we should use a builtin parser (class-based)
    if execution.hasParser(previewerCmd):
        parserFunc = _runBuiltinParser
    # ...or an external tool, script, whatever
    else:
        parserFunc = _runExternalParser
        
    try:
        # for profiling
        start = time.time()
        
        # run the appropriate parser
        result = parserFunc(previewerCmd, previewerCmds[1:], workingDir)
        
        # save the result to DB immediately
        _savePreviewResult(True, result, commandExecutionId, start)
             
        
    except SoftTimeLimitExceeded:
        if not ignoreErrors:
            _savePreviewResult(False, 'Timeout', commandExecutionId, start)
    except:
        if not ignoreErrors:
            _savePreviewResult(False, traceback.format_exc(), commandExecutionId, start)
            
        raise

def _runBuiltinParser(parserName, arguments, workingDir):
        parser = execution.createParserByName(parserName)
        
        # more than one argument? error
        if len(arguments) > 1:
            raise RuntimeError('The only (optional) argument, builtin parsers can use is the input file.')
        
        # exactly one, check if we need to replace the placeholders
        if len(arguments) == 1:
            arguments[0] = _replaceSpecialFileArgs(arguments[0], workingDir)
        
        # no argument, use stdout as default        
        if len(arguments) == 0:
            arguments.append(os.path.join(workingDir, STDOUT_FILE))
        seqId = _getSequenceId()
        
        # create parse-result
        result = parsers.LinePreviewResult(seqId)
        # parse it
        parser.parse(arguments[0], result)
    
        return result
    
def _replaceSpecialFileArgs(argument, workingDir):
    
    # if it is one of the magics, replace with the files in CWD
    if argument == '%stdout':
        return os.path.join(workingDir, STDOUT_FILE)
    if argument == '%stderr':
        return os.path.join(workingDir, STDERR_FILE)
    
    # if starts with $, replace it with the workingdir anyway
    if argument.startswith('$'):
        return os.path.join(workingDir, argument[1:])
    
    return argument

def _runExternalParser(parserName, arguments, workingDir):
    for i, arg in enumerate(arguments):
        arguments[i] = _replaceSpecialFileArgs(arg, workingDir)
    
    p = subprocess.Popen([parserName] + arguments,
                             stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=workingDir)
    (stdout, _) = p.communicate()
    
    result = parsers.LinePreviewResult(_getSequenceId())
    try:
        result.values = json.loads(stdout)
    except ValueError:
        result.values = stdout.splitlines()
    
    return result

def _savePreviewResult(success, result, commandExecutionId, starttime):
    """
    @param result: either the parser result or a string containing an error or whatever.
    """
    from modsched import models
    if success:
            defaults = {'sequence_id':result.sequence_id,
                        'data':{'type':result.previewType,
                                'values':result.values},
                        'runtime':time.time() - starttime,
                        'status':models.PreviewData.PREVIEW_SUCCESS,
                        }
            
    else:
        defaults = {'data':result,
                    'runtime':time.time() - starttime,
                    'status':models.PreviewData.PREVIEW_ERROR,
                   }
        
    (parseResult, created) = models.PreviewData.objects.get_or_create(pk=commandExecutionId,
                                                                              defaults=defaults)
            
        
    # if not created, update only if the sequence_id is higher
    # so we don't update with an older out-of-order parser-result
    if not created:
        
        # if successful and the existing one is newer, let's keep it
        if (success and
            parseResult.sequence_id >= defaults['sequence_id']):
            logger.debug('Ignoring out-of-order ParserResults due to sequence_id')
            return
        
        # in case of error or for updates, just overwrite.
        for name, value in defaults.iteritems():
            setattr(parseResult, name, value)
        parseResult.save()

def runParserForCommand(experimentId, execution):
    folder = store.getOrCreateExecutionFolder(experimentId, execution.sequence_id)
    return PreviewCommand.delay(commandExecutionId=execution.pk,
                                workingDir=folder,
                                previewer=execution.previewer,
                                ignoreErrors=False
                                )



@shared_task(bind=True,
             ignore_result=True,
             trail=False)
def RunCommandExecutionTask(self, executionId, experimentId):
    from modsched import models
    from modsched.services import experiment
    execution = models.CommandExecution.objects.get(pk=executionId)
    
    expandedString = experiment.expandInputFiles(execution.command_string, experimentId)

    execution.removePreviewData()
    
    
    # delete the old tasks
    if execution.command_task_id:
        celery_app.control.revoke(execution.command_task_id, terminate=True)
        
    cmdTask = RunCommand.delay(experimentId,
                                     execution.pk,
                                     execution.sequence_id,
                                     expandedString,
                                     execution.command.previewer)
    
    
    execution.command_task_id = cmdTask.task_id
    execution.save()
   
@receiver(post_save)
def _updateExperiment(sender, instance, created, **kwargs):
    from modsched import models
    from djcelery import models as djmodels
    if sender == models.Command:
        models.Experiment.objects.internalUpdate(instance.experiment_id)
        
    if sender == models.CommandExecution:
        models.Experiment.objects.internalUpdate(instance.command.experiment_id)
        
    if sender == models.PreviewData:
        models.Experiment.objects.internalUpdate(instance.command_execution.command.experiment_id)
        
    if sender == djmodels.TaskState:
        models.Experiment.objects.updateByTaskId(instance.task_id)


class TaskRouter(object):
    """
    Task router as defined by celery. 
    """
    def route_for_task(self, task, args=None, kwargs=None):
        if task == 'modsched.execution.tasks.RunCommand':
            return {'queue': 'commands',
                    'routing_key': 'commands'}
        elif task == 'modsched.execution.tasks.PreviewCommand':
            return {'queue': 'sidejobs',
                    'routing_key': 'sidejobs'}
        elif task == 'modsched.execution.tasks.RunCommandExecutionTask':
            return {'queue':'sidejobs',
                    'routing_key':'sidejobs'}
            
        return None
