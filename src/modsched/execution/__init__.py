from __future__ import absolute_import

from modsched.execution.parsers import prism
from modsched.execution import parsers

import os

import sys
# print sys.path
from celery import Celery

from django.conf import settings

app = Celery('modsched')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
    
    

PARSER_REGISTRY = {
    'prism-quantile':{'parser':prism.PrismParserQuantile
                      },
    'prism-4.2.beta1':{'parser':prism.PrismParserV4_2_beta1
                      },
    'prism-4.0.3':{'parser':prism.PrismParserV4_0_3
                      },
    'echo-parser':{'parser':parsers.EchoParser,
                   },
    'random':{'parser':parsers.RandomParser
              }
                   
}

def hasParser(name):
    return name in PARSER_REGISTRY

def createParserByName(name):
    return PARSER_REGISTRY[name]['parser']()

def getAllParserNames():
    return PARSER_REGISTRY.keys()