#!/usr/bin/python

'''
This module contains fake commands that are 
passed a log file that they'll simply print out 
in a determined timed way.
'''
import argparse
import os
import sys
import time

argparser = argparse.ArgumentParser()
argparser.add_argument("input", type=str)
argparser.add_argument('--interval', help="interval at which to print out the contents",
                       dest='interval', type=float, default=1)
argparser.add_argument('--chunksize', help="size in bytes of a chunk printed each *interval*",
                       dest='chunkSize', type=int, default=100)

if __name__ == '__main__':
    args = argparser.parse_args()
    if not os.path.isfile(args.input):
        raise AttributeError('invalid input file ' + args.input)
    
    with open(args.input, 'r') as inFile:
        while True:
            chunk = inFile.read(args.chunkSize)
            
            if len(chunk) == 0:
                break
            else:
                sys.stdout.write(chunk)
                sys.stdout.flush()
                
            
            time.sleep(args.interval)
