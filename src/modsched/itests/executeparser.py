'''

@author: eh14
'''
import sys
from modsched import execution
def _executeParser(parserName, logFile):
    if not execution.hasParser(parserName):
        print "***Unknown Parser. Existing parsers are {0}".format(execution.getAllParserNames())
        return 'Unknown parser ' + parserName
    
    parser = execution.createParserByName(parserName)
    
    result = parser.ParserResult()
    with open(logFile, 'r') as inFile:
        parser.parse(inFile, result)
        
    print result.toDict()
    
    return 0
    


if __name__ == '__main__':
    sys.exit(_executeParser(*sys.argv[1:]))
