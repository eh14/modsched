'''

@author: eh14
'''
import unittest
import requests
from modsched import itests

import os
import json



class BaseIntegrationTest(unittest.TestCase):
    BASE_URL = itests.SERVER_URL
    JSON_HEADERS = {'Content-type': 'application/json', 'Accept': 'application/json'}
        
    @classmethod
    def _get(cls, url):
        return cls.s.get(cls._url(url), headers=cls.JSON_HEADERS)
        
    @classmethod
    def _getJson(cls, url):
        r = cls._get(url)
        
        return json.loads(r.data)
    
    @classmethod
    def _post(cls, url, data=''):
        if not isinstance(data, str):
            data = json.dumps(data)
        return cls.s.post(cls._url(url), data=data,
                          headers=cls.JSON_HEADERS)
    
    @classmethod
    def _url(cls, url):
        return os.path.join(cls.BASE_URL, url)
    
    @classmethod
    def setUpClass(cls):
        # creating a new session
        cls.s = requests.Session()
        # logging in
        cls._post('api/v1/auth/login', {'username':'admin',
                                    'password':'admin'})
        
    @classmethod
    def tearDownClass(cls):
        # logging out
        cls._post('api/v1/auth/logout')
        

    def responseOk(self, response):
        self.assertEquals(200, response.status_code)