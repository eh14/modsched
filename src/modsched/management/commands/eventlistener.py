"""
"""
from __future__ import absolute_import, unicode_literals

from celery.bin import events

from djcelery.app import app
from djcelery.management.base import CeleryCommand

ev = events.events(app=app)


class Command(CeleryCommand):
    """Runs eventlistener that updates task results modsched-specifically.."""
    options = (CeleryCommand.options
               + ev.get_options()
               + ev.preload_options)
    help = 'Save Snapshots in the database, filtering parsing-results'

    def handle(self, *args, **options):
        """Handle the management command."""
        options['camera'] = 'modsched.execution.camera.ModSchedCamera'
        ev.run(*args, **options)