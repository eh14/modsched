"""
"""
from __future__ import absolute_import, unicode_literals
from django.core.management.base import BaseCommand, CommandError
import hglib
import os
from modsched import models
from modsched.services import store
import shutil

class Command(BaseCommand):
    """Setup the working repository."""
    help = 'Create and initialize the working directory'
    can_import_settings = True

    def handle(self, *args, **options):
        from django.conf import settings
        
        root = settings.MODSCHED_PROJECTDIR
        
        if not os.path.exists(root):
            os.makedirs(root)
        
        _repo = store.openOrInitRepo(root)
        
        self._createExperimentRepos(root)
        
    def _createExperimentRepos(self, rootPath):
        expPaths = []
        for exp in models.Experiment.objects.all():
            print "Syncing repo for experiment {exp.title} ({exp.id})".format(exp=exp)
            expRepo = store.setupExperimentRepo(exp)
            # save only the last folder name in the path
            expPaths.append(os.path.basename(expRepo.root().rstrip('/')))
           
        # check that all folders are actually repositories, remove the unnecessary
        for entry in os.listdir(rootPath):
            if entry not in expPaths:
                # ignore non-experiment entries, otherwise we end up
                # deleting additional files
                if not entry.startswith('experiment'):
                    continue
                shutil.rmtree(os.path.join(rootPath, entry))
                print "Removing folder {0} since experiment seems not to exist".format(entry)
                
        