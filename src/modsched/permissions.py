'''

@author: eh14
'''


from rest_framework import permissions
from modsched import models
from rest_framework.permissions import IsAuthenticatedOrReadOnly

class IsExperimentOwnerOrAdminOrReadonly(permissions.BasePermission):
    adminPermissions = permissions.IsAdminUser()
    
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        
        # check if it's an admin
        if self.adminPermissions.has_permission(request, view):
            return True
        
        if request.user:
            if isinstance(obj, models.Experiment):
                return obj.creator == request.user
            elif isinstance(obj, models.Command):
                return obj.experiment.creator == request.user
            elif isinstance(obj, models.CommandExecution):
                return obj.command.experiment.creator == request.user
            
            raise RuntimeError('Cannot determine permissions. Unsupported model type')
            
        return False

