from django.conf.urls import include, url
from django.contrib import admin

from rest_framework import routers

from authentication.views import LoginView, LogoutView
from modsched.views.index import IndexView
from modsched.views.experiments import ExperimentViewSet, CommandViewSet, \
    CommandExecutionViewSet
from modsched.views import experiments, dashboard
from modsched.views.executable import ExecutableViewSet, ArgumentViewSet

router = routers.DefaultRouter()
# sorry for the hack, but I really don't care about trailing slashes...
router.trailing_slash = '/?'

router.register(r'experiments', ExperimentViewSet, base_name='experiments')
router.register(r'executables', ExecutableViewSet, base_name='executables')
router.register(r'arguments', ArgumentViewSet, base_name='arguments')
router.register(r'commands', CommandViewSet, base_name='commands')
router.register(r'executions', CommandExecutionViewSet, base_name='executions')

urlpatterns = [

    url(r'^admin/?', include(admin.site.urls)),
    #
    # rest-api provided by the django-rest-tool
    url(r'^api/v1/', include(router.urls)),
    
    url(r'^api/v1/auth/login/?$', LoginView.as_view(), name='login'),
    url(r'^api/v1/auth/logout/?$', LogoutView.as_view(), name='logout'),
    url(r'^api/v1/dashboard/initial/?$', dashboard.InitialExperimentView.as_view(), name='dashboard_initial'),
    url(r'^api/v1/dashboard/experimentstate/?$', dashboard.ExperimentStatusView.as_view(), name='dashboard_state'),
    url(r'^api/v1/commands/(?P<commandId>[0-9]+)/start/?$', experiments.StartCommand.as_view(), name='command_start'),
    url(r'^api/v1/commands/(?P<commandId>[0-9]+)/stop/?$', experiments.StopCommand.as_view(), name='command_stop'),
    url(r'^api/v1/executions/(?P<executionId>[0-9]+)/runParser/?$', experiments.RunParser.as_view(), name='execution_runparser'),
    url(r'^api/v1/executions/(?P<executionId>[0-9]+)/start/?$', experiments.StartExecution.as_view(), name='execution_start'),
    url(r'^api/v1/executions/(?P<executionId>[0-9]+)/stop/?$', experiments.StopExecution.as_view(), name='execution_stop'),

    url(r'^api/v1/experiments/(?P<experimentId>[0-9]+)/makecommands/?$', experiments.MakeCommands.as_view(), name='experiments_makecommands'),
    url(r'^api/v1/experiments/(?P<experimentId>[0-9]+)/startAll/?$', experiments.StartAll.as_view(), name='experiments_startAll'),
    url(r'^api/v1/experiments/(?P<experimentId>[0-9]+)/stopAll/?$', experiments.StopAll.as_view(), name='experiments_stopAll'),
    url(r'^api/v1/experiments/(?P<experimentId>[0-9]+)/taskState/?$', experiments.TaskStateView.as_view(), name='experiments_taskstate'),
    url(r'^api/v1/experiments/(?P<experimentId>[0-9]+)/inputUpload/?$', experiments.InputUpload.as_view(), name='experiments_inputupload'),
    url(r'^api/v1/experiments/(?P<experimentId>[0-9]+)/repository/list/?$', experiments.ListRepositoryInput.as_view(), name='experiments_repo_list'),
    url(r'^api/v1/experiments/(?P<experimentId>[0-9]+)/repository/remove/(?P<filename>[\w.]{1,256})$', experiments.RemoveRepositoryInput.as_view(), name='experiments_repo_remove'),
    url(r'^api/v1/experiments/(?P<experimentId>[0-9]+)/repository/sync/?$', experiments.SyncRepository.as_view(), name='experiment_syncrepo'),
    
    url(r'^api/v1/utils/previewers/?$', experiments.PreviewerView.as_view(), name='previewers-list'),
    
    url(r'^.*$', IndexView.as_view(), name='index'),
]

