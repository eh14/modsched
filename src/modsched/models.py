from __future__ import absolute_import

from django.db import models
from django.contrib.auth.models import User
import jsonfield
from django.db.models.fields.related import ForeignKey
from modsched.services import commands
import itertools
from django.utils import timezone
class ExperimentManager(models.Manager):
    
    def internalUpdate(self, experimentId):
        self.filter(pk=experimentId).update(internal_update=timezone.now())

    def updateByTaskId(self, taskId):
        self.filter(command__executions__command_task_id=taskId).update(internal_update=timezone.now())
        
        
    def loadAllExperimentsAndTasks(self, changedSince=None, loadCreators=False):
        
        qs = self.prefetch_related('command_set__executions__command_task')
        if loadCreators:
            qs = qs.select_related('creator')
            
        if changedSince is not None:
            qs = qs.filter(internal_update__gt=changedSince)
            
        return qs.all()

class Experiment(models.Model):
    
    
    # generated ID, auto-increment
    id = models.AutoField(primary_key=True)
    
    # Title and description, purely informative
    title = models.CharField(max_length=64)
    description = models.CharField(max_length=256, blank=True)
    
    # user that created the experiment.
    creator = models.ForeignKey(User)
    
    creation_date = models.DateTimeField(auto_now_add=True)
    start_date = models.DateTimeField(null=True)
    finish_date = models.DateTimeField(null=True)
    
    #
    # tracker for internal updates
    internal_update = models.DateTimeField(null=False, auto_now_add=True, blank=False)
    objects = ExperimentManager()
    def getAllExecutions(self):
        for cmd in self.command_set.prefetch_related('executions').all():
            for execution in cmd.executions.all():
                yield execution
                
    def save(self, *args, **kwargs):
        self.internal_update = timezone.now()
        
        models.Model.save(self, *args, **kwargs)
    
class Executable(models.Model):
    id = models.AutoField(primary_key=True)
    
    path = models.CharField(max_length=256)
    description = models.CharField(max_length=256, null=False, blank=True)
    
class Argument(models.Model):
    
    executable = ForeignKey(Executable, on_delete=models.CASCADE)
     
    name = models.CharField(max_length=64, null=False, blank=False)
    
    choices = jsonfield.JSONField(default=[], help_text='List of suggestions as value arguments. May be chosen depending on allow_custom')
    
    isDefault = models.BooleanField(default=False, null=False, blank=False, help_text='If true, this argument will be added by default when used in a command')
    
class CommandManager(models.Manager):
    
    pass
#     def create(self, **data):
#         command = models.Manager.create(self, **data)
#         
#         command.replaceCommandExecutions(noDelete=True)
#         
#         return command
    
class Command(models.Model):
    """
    Models all commands executed by an experiment
    """
    
    id = models.AutoField(primary_key=True)
    experiment = models.ForeignKey(Experiment, on_delete=models.CASCADE)
     
    executable = models.CharField(max_length=64, blank=False, null=False)
    
    #
    # if set to true, certain syntax of arguments will expand to multiple
    # CommandExecution entries.
    arguments_expand = models.BooleanField(default=True, blank=False, null=False)
    
    # arguments for the executable. This includes arguments specified
    # by the executable. If the arguments are renamed etc, they'll be considered
    # normal uncategorized arguments.
    arguments = jsonfield.JSONField(default=[], max_length=1024)
    
    previewer = models.CharField(max_length=256, blank=False, null=True)
    previewer_series = models.CharField(max_length=256, blank=False, null=True)
    
    objects = CommandManager()
    
    def getExpandedExecutionCommands(self):
        """
        returns a list of execution commands, depending on expansion syntax and if expansion 
        is activated in general.
        """
        if not self.arguments_expand:
            return ['{0} {1}'.format(self.executable,
                                ' '.join(self.arguments))]
        else:
            return commands.expandCommand(self.executable, ' '.join(self.arguments))
        
    def save(self, *args, **kwargs):
        isNew = self.pk is None
        
        
        if not self.previewer_series:
            self.previewer_series = self._generatePreviewerSeries()
        
        ret = models.Model.save(self, *args, **kwargs)
        
        self.replaceOrUpdateCommandExecutions(isNew)
        
        return ret
    
    def _generatePreviewerSeries(self):
        return "{cmd}-exp-{expId}".format(cmd=self.executable,
                                    expId=self.experiment_id)
    
    def replaceOrUpdateCommandExecutions(self, noDelete=False):
        executionCommands = self.getExpandedExecutionCommands()
        if self.executions.all().count() == len(executionCommands):
            self.updateCommandExecutions()
        else:
            self.replaceCommandExecutions(noDelete)
            
    def replaceCommandExecutions(self, noDelete=False):
        """
        Creates a CommandExecution entity for all executions by this command..
        """
        if not noDelete:
            self.executions.all().delete()
        
        nextSeqId = CommandExecution.objects.getMaxSequenceIdForExperiment(self.experiment_id)

        # if None, no execution exists, start at 1. 
        # Increment by 1 otherwise
        if not nextSeqId:
            nextSeqId = 1
        else:
            nextSeqId += 1
        
        commandExecutions = self.getExpandedExecutionCommands()
        for (seqId, expanded) in enumerate(commandExecutions, nextSeqId):
            CommandExecution.objects.create(command=self,
                sequence_id=seqId,
                command_string=expanded,
                previewer=self.previewer,
                previewer_series=self.previewer_series
                )
        
        return self
    
    def updateCommandExecutions(self):
        expandedCommands = self.getExpandedExecutionCommands()
        executions = self.executions.order_by('sequence_id').all()
        
        assert len(expandedCommands) == len(executions)
        for (cmd, exe) in itertools.izip(expandedCommands, executions):
            exe.command_string = cmd
            exe.command_task = None
            exe.previewer = self.previewer
            exe.previewer_series = self.previewer_series
            exe.save()
    
class CommandExecutionManager(models.Manager):
    
    def getAllForExperiment(self, experimentId):
        return self.filter(command__experiment_id=experimentId)
    
        
    def getMaxSequenceIdForExperiment(self, experimentId):
        return self.filter(command__experiment_id=experimentId).aggregate(num=models.Max('sequence_id'))['num']
    
    
    def getExecutionPrefetchExperiment(self, executionId):
        return self.filter(pk=executionId).select_related('command__experiment').get()
    
class CommandExecution(models.Model):
    id = models.AutoField(primary_key=True)
    
    command = models.ForeignKey(Command, on_delete=models.CASCADE, related_name="executions")
    
    # sequence ID within the command so we can match the command executions 
    # between different runs even when the command is modified.
    sequence_id = models.PositiveIntegerField(blank=False, null=False)
    #
    # the command that is finally being executed
    command_string = models.CharField(max_length=1024)
    
    command_task = models.OneToOneField('djcelery.TaskState',
                             db_constraint=False,  # No constraint since the task might
                              # be created later than the assignment
                             to_field='task_id',
                            unique=True, null=True,
                            on_delete=models.SET_NULL)
    
    previewer = models.CharField(max_length=256, blank=False, null=True)
    previewer_series = models.CharField(max_length=256, blank=False, null=True)
    
    #
    # variable that maps the preview-parser's output to the axes of the
    # preview chart.
    # Default is: x:x, y:y, series:series.
    preview_mapping = jsonfield.JSONField(default={'x':'x',
                                                   'y':'y',
                                                   'series':'series'},
                                          max_length=256)
    
    objects = CommandExecutionManager()
    
    def getPreviewData(self):
        try:
            return self.preview_data
        except PreviewData.DoesNotExist:
            return None
        
    def __str__(self):
        return 'Command Execution seq: {c.sequence_id}, prev:{c.previewer}'.format(c=self)
    
    
    def removePreviewData(self):
        """
        Removes the preview data for this execution, if exists.
        """
        PreviewData.objects.filter(pk=self.pk).delete()
            
class PreviewData(models.Model):
    PREVIEW_ERROR = 'error'
    PREVIEW_SUCCESS = 'success'
    """
    Stores the (newest) result of a commandstep
    """
    command_execution = models.OneToOneField(CommandExecution,
                                   primary_key=True,
                                   related_name='preview_data',
                                   on_delete=models.CASCADE)
    
    #
    # Sequence ID of preview data. This ID is only used to ignore out-of-order updates
    sequence_id = models.BigIntegerField(default=0)
    data = jsonfield.JSONField(default={})
    
    status = models.CharField(max_length=32, default=PREVIEW_SUCCESS, null=False, blank=False)
    
    runtime = models.FloatField(null=True, help_text='in seconds if task succeeded')

