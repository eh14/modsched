'''

@author: eh14
'''


from rest_framework import viewsets, views, status, generics
from rest_framework.response import Response
from rest_framework import permissions as rfpermissions


from modsched.models import Experiment, Command, CommandExecution
from modsched.serializers import ExperimentSerializer, CommandSerializer, \
    CommandExecutionSerializer
from modsched.services import commands, store, experiment

from __builtin__ import NotImplementedError
from modsched import serializers, execution, models, celery_app, permissions
from rest_framework.parsers import FileUploadParser
from django.http.response import Http404
from django.views.decorators.http import condition
from django.utils.decorators import method_decorator

class ExperimentViewSet(viewsets.ModelViewSet):
    queryset = Experiment.objects.order_by('-creation_date')
    serializer_class = ExperimentSerializer
    
    permission_classes = (permissions.IsExperimentOwnerOrAdminOrReadonly, rfpermissions.IsAuthenticatedOrReadOnly)

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)
        return viewsets.ModelViewSet.perform_create(self, serializer)
    
def getExpLastModified(request, *args, **kwargs):
    
    expId = request.QUERY_PARAMS.get('experiment_id', None)
    if not expId:
        return 0
    
    return Experiment.objects.get(pk=expId).internal_update

class CommandViewSet(viewsets.ModelViewSet):
    queryset = Command.objects.order_by('id')
    serializer_class = CommandSerializer
    
    permission_classes = (permissions.IsExperimentOwnerOrAdminOrReadonly,)
    
    def list(self, request):
        expId = request.QUERY_PARAMS.get('experiment_id', None)
        if expId is None:
            raise Http404('No or invalid Experiment-ID given')
        
        serializer = self.get_serializer(self.queryset.filter(experiment_id=expId), many=True)
        return Response(serializer.data)
    
class CommandExecutionViewSet(viewsets.ModelViewSet):
    queryset = CommandExecution.objects.order_by('sequence_id')
    serializer_class = CommandExecutionSerializer
    
    permission_classes = (permissions.IsExperimentOwnerOrAdminOrReadonly,)
    
    @method_decorator(condition(last_modified_func=getExpLastModified))
    def list(self, request):
        expId = request.QUERY_PARAMS.get('experiment_id', None)
        if expId is None:
            raise Http404('No or invalid Experiment-ID given')
        
        serializer = self.get_serializer(self.queryset.filter(command__experiment_id=expId), many=True)
        return Response(serializer.data)
    
class MakeCommands(views.APIView):
    
    def post(self, request, experimentId):
        
        command = request.data.get('command', '')
        args = request.data.get('arguments', '')
        
        if isinstance(args, list):
            args = ' '.join(args)
            
        return  Response(commands.expandCommand(command, args))
    
class SyncRepository(views.APIView):
    
    permission_classes = (permissions.IsExperimentOwnerOrAdminOrReadonly,)
    
    def put(self, request, experimentId):
        msg = request.data.get('message', '')
        # force = request.data.get()
        user = request.user
        
        if user:
            username = user.username
        else:
            username = 'system'
        
        store.syncExperimentRepo(experimentId, message=msg, user=username)
        
        return Response(data='Ok')
    
class _ExecutionView(views.APIView):
    
    permission_classes = (permissions.IsExperimentOwnerOrAdminOrReadonly,)
    
    def post(self, request, **kwargs):
        try:
            itemId = kwargs.itervalues().next()
            item = self.queryset.get(pk=itemId)
            self.check_object_permissions(request, item)
            self._run(itemId)
            return Response(data='Ok')
        
        
        except Experiment.DoesNotExist:
            return Response(data='Invalid Experiment ID', status=status.HTTP_404_NOT_FOUND)
        except Command.DoesNotExist:
            return Response(data='Invalid Command ID', status=status.HTTP_404_NOT_FOUND)
        except CommandExecution.DoesNotExist:
            return Response(data='Invalid CommandExecution ID', status=status.HTTP_404_NOT_FOUND)
    def _run(self, expId):
        """
        Executed when the ID is correctly passed.
        """
        
        raise NotImplementedError()
    
class StartCommand(_ExecutionView):
    queryset = models.Command.objects
    
    def _run(self, commandId):
        experiment.startSingleCommand(commandId)

class StopCommand(_ExecutionView):
    
    queryset = models.Command.objects
    
    
    def _run(self, commandId):
        experiment.stopSingleCommand(commandId)
        
        
class StartExecution(_ExecutionView):
    queryset = models.CommandExecution.objects
    
    def _run(self, executionId):
        experiment.startSingleExecution(executionId)
        
class StopExecution(_ExecutionView):
    queryset = models.CommandExecution.objects
    
    def _run(self, executionId):
        experiment.stopSingleExecution(executionId)
    
class StartAll(_ExecutionView):
    """
    Runs all commands of an experiment.
    """
    
    queryset = models.Experiment.objects
    
    def _run(self, expId):
        experiment.startById(expId)
        
class StopAll(_ExecutionView):
    """
    Stops all commands of an experiment
    """
    
    queryset = models.Experiment.objects
    
    def _run(self, expId):
        experiment.stopById(expId)
        
        
class RunParser(views.APIView):
    
    permission_classes = (permissions.IsExperimentOwnerOrAdminOrReadonly,)

    queryset = models.CommandExecution.objects
   
    def post(self, request, **kwargs):
        try:
            
            itemId = kwargs['executionId']
            item = self.queryset.get(pk=itemId)
            self.check_object_permissions(request, item)
            
            experiment.runParserForExecution(item)
            return Response(data='Ok')
        
        except CommandExecution.DoesNotExist:
            return Response(data='Invalid Execution ID, cannot reparse', status=status.HTTP_404_NOT_FOUND)
        
class PreviewerView(views.APIView):
    def get(self, request):
        return Response(execution.PARSER_REGISTRY.keys())
        
class TaskStateView(views.APIView):
    
    def get(self, request, experimentId):
        query = CommandStep.objects.filter(experiment_id=experimentId).order_by('id')
        query = query.select_related('command_task').select_related('parse_result')
        return Response(serializers.CommandStepSerializer(query, many=True).data)
    

class InputUpload(views.APIView):
    parser_classes = (FileUploadParser,)
    
    permission_classes = (permissions.IsExperimentOwnerOrAdminOrReadonly,)
    
    def post(self, request, experimentId, *args, **kwargs):
        
        # might raise a permission-denied exception
        self.check_object_permissions(request, models.Experiment.objects.get(pk=experimentId))
        
        # write it to the directory
        inputFile = request.data['file']
        store.addInput(experimentId, inputFile.name, inputFile)
        
        return Response(status=204)
    
    
class ListRepositoryInput(views.APIView):
    
    def get(self, request, experimentId):
        return Response(store.listInput(experimentId))
    
class RemoveRepositoryInput(views.APIView):
    
    permission_classes = (permissions.IsExperimentOwnerOrAdminOrReadonly,)
    
    
    def put(self, request, experimentId, filename):
        
        # might raise a permission-denied exception
        self.check_object_permissions(request, models.Experiment.objects.get(pk=experimentId))
        
        
        store.removeInput(experimentId, filename)
        return Response(status=204)

