'''

@author: eh14
'''
from rest_framework import viewsets
from modsched import models, serializers

class ExecutableViewSet(viewsets.ModelViewSet):
    queryset = models.Executable.objects.order_by('path')
    serializer_class = serializers.ExecutableSerializer
    
class ArgumentViewSet(viewsets.ModelViewSet):
    queryset = models.Argument.objects.order_by('name')
    serializer_class = serializers.ArgumentSerializer
