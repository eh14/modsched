'''

@author: eh14
'''
from rest_framework import views, serializers
from rest_framework.response import Response
from modsched.serializers import dashboard
from modsched import models
from django.utils import timezone, http
from celery import states
import datetime

class InitialExperimentView(views.APIView):
    def get(self, request):
        
        exps = models.Experiment.objects.loadAllExperimentsAndTasks(loadCreators=True)
        
        s = dashboard.ExperimentSerializer(exps, many=True)
        
        return Response(s.data)
    
class ModelAdapter(object):
    """
    Convenience-wrapper-model that creates instance-attributes for all constructor-kwargs.
    It serves as a adapter to initialize the serializers without having real models,
    since the models do not exist.
    """
    def __init__(self, **kwargs):
        for k, v in kwargs.iteritems():
            setattr(self, k, v)
        
class ExperimentStatusView(views.APIView):
    def get(self, request):
        is_modified_since = request.META.get('HTTP_IF_MODIFIED_SINCE', None)
        if is_modified_since is not None:
            is_modified_since = http.parse_http_date_safe(is_modified_since)
        if is_modified_since is not None:
            is_modified_since = datetime.datetime.fromtimestamp(is_modified_since)
        print is_modified_since
        exps = models.Experiment.objects.loadAllExperimentsAndTasks(changedSince=is_modified_since)
        
        # buffer for all experiment-models
        expModels = []
        
        # for each experiment, collect all execution-models
        for exp in exps:
            executions = []
            
            for cmd in exp.command_set.all():
                for exe in cmd.executions.all():
                    if exe.command_task:
                        state = exe.command_task.state
                        tstamp = exe.command_task.tstamp
                    else:
                        state = states.PENDING
                        tstamp = None
                        
                    # the fields here must match the fields in dashboard.ExecutionStateSerializer
                    executions.append(ModelAdapter(id=exe.pk,
                                                   state=state,
                                                   task_id=exe.command_task_id,
                                                   tstamp=tstamp))
                    
                # ... and append a new model-adapter using the execution-model-adapters
            expModels.append(ModelAdapter(id=exp.pk, executions=executions))
            
        # combine everything in the appropriate serializers
        return Response(dashboard.ExperimentStateSerializer(expModels, many=True).data)
