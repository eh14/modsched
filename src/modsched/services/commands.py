'''

@author: eh14
'''
import itertools
import re


_flexArg = re.compile('\<([\s0-9:]+)\>')
def expandCommand(executable, arguments):
    flexArgs = _flexArg.findall(arguments)
    expandedArgs = [_expandFlexArg(a) for a in flexArgs]
    product = itertools.product(*expandedArgs)

    commands = []
    for replaceArgs in product:
        replaceArgs = list(replaceArgs)
        replaced = _flexArg.sub(lambda m: str(replaceArgs.pop(0)), arguments)
        commands.append('%s %s' % (executable, replaced))

    return commands

def _expandFlexArg(arg):
    elements = [a.strip() for a in arg.split(':')]
    
    if len(elements) == 1:
        return range(int(elements[0]) + 1)
    elif len(elements) == 2:
        return range(int(elements[0]), int(elements[1]) + 1)
    elif len(elements) == 3: 
        return range(int(elements[0]), int(elements[2]) + 1, int(elements[1]))
    else:
        raise Exception('invalid iterator syntax')

