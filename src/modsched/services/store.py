'''
Service functions handling the persistent storage of experiment data.

@author: eh14
'''
import logging
import os

from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

import hglib
import shutil

logger = logging.getLogger(__name__)


def _getRepoPath():
    from django.conf import settings
    return settings.MODSCHED_PROJECTDIR

def openOrInitRepo(path):
    try:
        if not os.path.exists(os.path.join(path, '.hg')):
            repo = hglib.init(path)
            return repo.open()
        else:
            return hglib.open(path)
    except hglib.error.ServerError:
        repo = hglib.init(path)
        repo.open()
        
        return repo

def _expFolderName(experimentId, *suffixes):
    return os.path.join(_getRepoPath(), "experiment-{0}".format(experimentId), *suffixes)

def _expInputFolderName(experimentId, *suffixes):
    return _expFolderName(experimentId, 'input', *suffixes)

def setupExperimentRepo(experiment):
    """
    sets up the folder, repository and all necessary directories for the 
    experiment.
    
    This operation is idempotent.
    """
    
    folder = _expFolderName(experiment.pk)
    
    if not os.path.exists(folder):
        os.makedirs(folder)
    repo = openOrInitRepo(folder)
    
    assert repo.server, "Repo not properly initialized"

    inputFolder = _expInputFolderName(experiment.pk)
    if not os.path.exists(inputFolder):
        os.makedirs(inputFolder)
        
    with open(os.path.join(repo.root(), 'INFO'), 'w') as info:
        print >> info, """ Repository for Experiment {exp.title}
{exp.description}""".format(exp=experiment)

    with open(os.path.join(inputFolder, 'INFO'), 'w') as info:
        print >> info, "This folder contains all input files for the experiment."
    
    syncExperimentRepo(experiment.pk, repo, 'Initial commit')
    
    return repo
    
def syncExperimentRepo(experimentId, repo=None, message='', user="system"):
    """
    Synchronizes the experiment repository by adding all files, updating and commiting 
    if there are changes.
    """
    if repo is None:
        folder = _expFolderName(experimentId)
        repo = openOrInitRepo(folder)
    logger.debug("Syncing Repository {0}".format(experimentId))
    
    message = str(message).strip()
    if not message:
        message = 'no message provided'
    
    # sync all files
    repo.addremove()
    
    # update, someone might have changed it externally
    repo.update()
    
    # commit if there is something to commit
    status = repo.status(modified=True, added=True, removed=True)
    logger.debug("Status: {0}".format(status))
    if len(status):
        logger.debug("Committing. Message: {msg}, user: {user}".format(msg=message, user=user))
        repo.commit(message, user=user)
    
def getOrCreateExecutionFolder(experimentId,
                               executionSequenceId):
    path = _expFolderName(experimentId, "execution-{0}".format(executionSequenceId))
    if not os.path.exists(path):
        os.makedirs(path)
        
    return path
    

def getInputFolderName(experimentId):
    return _expInputFolderName(experimentId)

def listInput(experimentId):
    return [item for item in os.listdir(_expInputFolderName(experimentId)) if item != 'INFO']
    
def addInput(experimentId, name, inputFile):
    
    BUFSIZE = 1024
    with open(_expInputFolderName(experimentId, inputFile.name), 'w') as out:
        while True:
            d = inputFile.read(BUFSIZE)
            if not d:
                break
            out.write(d)


def removeInput(experimentId, name):
    os.remove(_expInputFolderName(experimentId, name))


#
# Signal receiver functions 
# 

@receiver(post_save)
def _updateExperimentRepoHandler(sender, instance, created, **kwargs):
    from modsched import models
    
    if not created or sender != models.Experiment:
        return
    
    setupExperimentRepo(instance)

@receiver(post_delete)
def _removeExperimentRepoHandler(sender, instance, **kwargs):
    from modsched import models
    
    if sender != models.Experiment:
        return
    
    expPath = _expFolderName(sender.pk)
    if not os.path.exists(expPath):
        logger.warn('Repo for experiment that was just deleted is not present {exp.title} ({exp.id})'.format(instance))
        return
    
    shutil.rmtree(expPath)
    
    
