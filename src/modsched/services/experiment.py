'''

@author: eh14
'''
import djcelery.models as djmodels
from modsched import models
from modsched.execution import tasks
from modsched import celery_app
from modsched.services import store
import re
import os
import time
from celery.app import shared_task

def stop(ex):
    for execution in ex.getAllExecutions():
        if execution.command_task_id:
            celery_app.control.revoke(execution.command_task_id, terminate=True)

def stopById(expId):
    ex = models.Experiment.objects.get(pk=expId)
    stop(ex)
        
def startById(expId):
    ex = models.Experiment.objects.get(pk=expId)
    return start(ex)
    
def start(ex):
    
    # collect the created tasks.
    # Normally they will be asyncronous results but in unit-tests they
    # will contain the results since executed immediately
    async_result = []
    
    for execution in ex.getAllExecutions():
        cmdTask = tasks.RunCommandExecutionTask.delay(execution.pk, ex.id)
        
        async_result.append(cmdTask)
        
    return async_result

def runParserForExecution(execution):
    """
    Explicitly runs the parser for an execution result.
    This results in the current preview_data being removed, so that
    failures will not silently be ignored by the camera.
    """
    
    tasks.runParserForCommand(execution.command.experiment_id, execution)
    
def startSingleCommand(commandId):
    cmd = models.Command.objects.get(pk=commandId)
    return [tasks.RunCommandExecutionTask.delay(exe.pk, cmd.experiment_id) for exe in cmd.executions.all()]

def stopSingleCommand(commandId):
    cmd = models.Command.objects.get(pk=commandId)
    for execution in cmd.executions.all():
        if execution.command_task_id:
            celery_app.control.revoke(execution.command_task_id, terminate=True)

def startSingleExecution(executionId):
    exe = models.CommandExecution.objects.getExecutionPrefetchExperiment(executionId)
    
    return tasks.RunCommandExecutionTask.delay(exe.pk, exe.command.experiment_id)

def stopSingleExecution(executionId):
    exe = models.CommandExecution.objects.getExecutionPrefetchExperiment(executionId)
    if exe.command_task_id:
        celery_app.control.revoke(exe.command_task_id, terminate=True)
        
        
_inputPattern = re.compile('\$([\w\.]+)')

def expandInputFiles(commandString, experimentId):
    inputFiles = store.listInput(experimentId)
    inputFolderName = store.getInputFolderName(experimentId)
    
    def _replace(m):
        name = m.group(1)
        if name in inputFiles:
            return os.path.join(inputFolderName, name)
        else:
            return m.group(0)
        
    return _inputPattern.sub(_replace, commandString)
    
