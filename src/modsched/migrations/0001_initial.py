# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import jsonfield.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('djcelery', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Argument',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('choices', jsonfield.fields.JSONField(default=[], help_text=b'List of suggestions as value arguments. May be chosen depending on allow_custom')),
                ('isDefault', models.BooleanField(default=False, help_text=b'If true, this argument will be added by default when used in a command')),
            ],
        ),
        migrations.CreateModel(
            name='Command',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('executable', models.CharField(max_length=64)),
                ('arguments_expand', models.BooleanField(default=True)),
                ('arguments', jsonfield.fields.JSONField(default=[], max_length=1024)),
                ('previewer', models.CharField(max_length=256, null=True)),
                ('previewer_series', models.CharField(max_length=256, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='CommandExecution',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('sequence_id', models.PositiveIntegerField()),
                ('command_string', models.CharField(max_length=1024)),
                ('previewer', models.CharField(max_length=256, null=True)),
                ('previewer_series', models.CharField(max_length=256, null=True)),
                ('preview_mapping', jsonfield.fields.JSONField(default={b'y': b'y', b'x': b'x', b'series': b'series'}, max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='Executable',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('path', models.CharField(max_length=256)),
                ('description', models.CharField(max_length=256, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Experiment',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=64)),
                ('description', models.CharField(max_length=256, blank=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('start_date', models.DateTimeField(null=True)),
                ('finish_date', models.DateTimeField(null=True)),
                ('internal_update', models.DateTimeField(auto_now_add=True)),
                ('creator', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='PreviewData',
            fields=[
                ('command_execution', models.OneToOneField(related_name='preview_data', primary_key=True, serialize=False, to='modsched.CommandExecution')),
                ('sequence_id', models.BigIntegerField(default=0)),
                ('data', jsonfield.fields.JSONField(default={})),
                ('status', models.CharField(default=b'success', max_length=32)),
                ('runtime', models.FloatField(help_text=b'in seconds if task succeeded', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='commandexecution',
            name='command',
            field=models.ForeignKey(related_name='executions', to='modsched.Command'),
        ),
        migrations.AddField(
            model_name='commandexecution',
            name='command_task',
            field=models.OneToOneField(db_constraint=False, null=True, on_delete=django.db.models.deletion.SET_NULL, to_field=b'task_id', to='djcelery.TaskState'),
        ),
        migrations.AddField(
            model_name='command',
            name='experiment',
            field=models.ForeignKey(to='modsched.Experiment'),
        ),
        migrations.AddField(
            model_name='argument',
            name='executable',
            field=models.ForeignKey(to='modsched.Executable'),
        ),
    ]
