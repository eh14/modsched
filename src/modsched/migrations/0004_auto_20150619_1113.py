# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('modsched', '0003_remove_command_executable'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='command',
            name='executable1',
        ),
        migrations.AddField(
            model_name='command',
            name='executable',
            field=models.CharField(default='', max_length=64),
            preserve_default=False,
        ),
    ]
