# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('modsched', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='command',
            name='executable1',
            field=models.CharField(default=b'', max_length=64),
        ),
    ]
