(function() {
    'use strict';

    angular.module('modsched', [ 'modsched.config', //
    'modsched.routes',//
    'modsched.authentication',//
    'modsched.dashboard',//
    'modsched.experiments', //
    'modsched.executables', //
    'modsched.preview', //
    'modsched.utils',//
    'modsched.layout',//
    'ui.bootstrap',// 
    'ngCookies', //
    'angularFileUpload',//
    'ngDialog',//
    'ngBusy',//
    ]);

    angular.module('modsched.config', []);

    angular.module('modsched.routes', [ 'ngRoute' ]);

})();
