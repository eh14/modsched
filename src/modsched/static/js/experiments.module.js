(function() {
    'use strict';

    angular
            .module('modsched.experiments', [
                    'modsched.experiments.controllers',
                    'modsched.experiments.directives',
                    'modsched.experiments.service' ]);

    angular.module('modsched.experiments.controllers', []);

    angular.module('modsched.experiments.directives', []);

    angular.module('modsched.experiments.service', []);

    var controllers = angular.module('modsched.experiments.controllers');

    /** *** Service **** */
    angular.module('modsched.experiments.service').factory('ExperimentService',
            ExperimentService);

    ExperimentService.$inject = [ '$http', '$location', 'ngDialog', 'Snackbar' ];

    function ExperimentService($http, $location, ngDialog, Snackbar) {

        function showEditDialog(action, initialExperiment, saveCallback) {
            return ngDialog
                    .open({
                        template : 'static/templates/experiments/exp_edit_template.html',
                        controller : [
                                '$scope',
                                function($scope) {
                                    $scope.action = action;
                                    $scope.experiment = angular
                                            .copy($scope.ngDialogData.experiment);
                                    $scope.error = null;
                                    var vm = this;
                                    $scope.save = function() {
                                        saveCallback($scope);
                                    };
                                } ],
                        data : {
                            experiment : initialExperiment
                        },
                    });
        }

        function create(experiment) {
            return $http.post(window.urls('experiments_list'), experiment, {
                name : 'createExperiment'
            });
        }

        function update(experiment) {
            return $http.put(window.urls('experiments_detail', experiment.id),
                    experiment);
        }

        function createCommand(experimentId, data) {
            return $http.post(window.urls('commands_list', experimentId), data,
                    {
                        name : 'modifyCommand'
                    });
        }

        return {
            all : function() {
                return $http.get(window.urls('experiments_list'));
            },
            create : create,
            get : function(id) {
                return $http.get(window.urls('experiments_detail', id));
            },

            getCommands : function(expId) {
                return $http.get(window.urls('commands_list', expId));
            },
            getCommandDetails : function(commandId) {
                return $http.get(window.urls('commands_detail', commandId));
            },
            getCommandExecutions : function(expId, lastMod) {

                if (!lastMod) {
                    lastMod = 0;
                }
                return $http.get(window.urls('executions_list', expId), {
                    headers : {
                        "if-modified-since" : lastMod
                    }
                });
            },
            getPreviewers : function() {
                return $http.get(window.urls('previewers_list'));
            },
            update : update,
            makeCommands : function(experimentId, command, args) {
                return $http.post(window.urls('experiments_makecommands',
                        experimentId), {
                    command : command,
                    arguments : args
                }, {
                    name : 'previewCommands'
                });
            },
            startAll : function(experiment) {
                return $http.post(window.urls('experiments_startAll',
                        experiment.id), null, {
                    name : 'startAll'
                });
            },
            stopAll : function(experiment) {
                return $http.post(window.urls('experiments_stopAll',
                        experiment.id), null, {
                    name : 'stopAll'
                });
            },
            startCommand : function(commandId) {
                return $http.post(window.urls('command_start', commandId),
                        null, {
                            name : 'startCommand'
                        });
            },
            stopCommand : function(commandId) {
                return $http.post(window.urls('command_stop', commandId), null,
                        {
                            name : 'stopCommand'
                        });
            },
            startExecution : function(executionId) {
                return $http.post(window.urls('execution_start', executionId));
            },
            stopExecution : function(executionId) {
                return $http.post(window.urls('execution_stop', executionId));
            },
            getTaskState : function(experiment) {
                return $http.get(window.urls('experiments_taskstate',
                        experiment.id));
            },
            repoListInput : function(experimentId) {
                return $http.get(window.urls('experiments_repo_list',
                        experimentId));
            },
            repoRemoveInput : function(experimentId, filename) {
                return $http.put(window.urls('experiments_repo_remove',
                        experimentId, filename));
            },
            addExperiment : function() {
                showEditDialog('Add', {}, function($scope) {
                    create($scope.experiment).then(
                            function(data) {
                                $scope.closeThisDialog(true);
                                $location.path('experiments/' + data.data.id
                                        + '/show');
                            }, function(data) {
                                $scope.error = "Error creating Experiment";
                            });
                });
            },
            editExperiment : function(experiment) {
                return showEditDialog('Edit', experiment, function($scope) {
                    return update($scope.experiment).then(function() {
                        $scope.closeThisDialog(true);
                    }, function(data) {
                        $scope.error = "Error updating Experiment";
                    });
                }).closePromise;
            },

            createCommand : createCommand,

            deleteCommand : function(commandId) {
                return $http['delete'](window
                        .urls('commands_detail', commandId));
            },

            updateCommand : function(commandId, command) {
                return $http.put(window.urls('commands_detail', commandId),
                        command, {
                            name : 'modifyCommand'
                        });
            },

            translateExecutionState : function(state) {
                if (_.contains([ null, 'PENDING' ], state)) {
                    return "IDLE";
                }
                if (_.contains([ 'RECEIVED', 'RETRY' ], state)) {
                    return 'WAITING';
                }
                if (state == 'STARTED') {
                    return 'RUNNING';
                }

                if (state == 'REVOKED') {
                    return 'STOPPED';
                }

                if ('SUCCESS' == state) {
                    return 'SUCCESS';
                }
                if (_.contains([ 'FAILURE', 'REJECTED', 'IGNORED' ], state)) {
                    return 'FAILURE';
                }

                throw 'Unhandled state' + state;
            },
            getExecutionRuntime : function(execution) {
                if (execution.command_task) {
                    if (execution.command_task.runtime != null) {
                        return execution.command_task.runtime.toFixed(2)
                                + ' seconds';
                    }
                    if (execution.translatedState == 'RUNNING'
                            && execution.command_task.tstamp != null) {
                        return moment(execution.command_task.tstamp).fromNow(
                                true);
                    }
                }
                return "N/A";

            },

            updatePreviewSettings : function(executionId, data) {
                return $http.put(window.urls('executions_detail', executionId),
                        data);
            },

            runParser : function(executionId) {
                return $http.post(window.urls('execution_runparser',
                        executionId));
            },

            syncRepository : function(experimentId, message, force) {
                return $http.put(window.urls('experiment_syncrepo',
                        experimentId), {
                    message : message,
                    force : force
                });
            },

        };
    }

    controllers.controller('ExperimentListController', [
            '$scope',
            'ExperimentService',
            'Snackbar',
            function($scope, ExperimentService, Snackbar) {
                var vm = this;

                $scope.experiments = []

                ExperimentService.all().then(experimentsSuccessFn,
                        experimentsErrorFn);

                function experimentsSuccessFn(data, status, headers, config) {
                    $scope.experiments = data.data;
                }

                function experimentsErrorFn(data, status, headers, config) {
                    Snackbar.error('fetching experiment list failed '
                            + data.statusText);
                }

                vm.createExperiment = function() {
                    ExperimentService.addExperiment();
                };

                $scope.$on('experiment.created', function(event, experiment) {
                    vm.experiments.unshift(experiment);
                });

                $scope.$on('experiment.created.error', function() {
                    vm.experiments.shift();
                });
            } ]);

    controllers.controller('ExperimentShowController', [ '$scope', '$timeout',
            '$location', '$routeParams', 'ExperimentService', 'Snackbar',
            'DialogService', 'ngDialog', 'PreviewService',
            ExperimentShowController ]);

    function ExperimentShowController($scope, $timeout, $location,
            $routeParams, ExperimentService, Snackbar, DialogService, ngDialog,
            PreviewService) {
        var vm = this;
        $scope.taskStates = [];
        $scope.experimentProgress = 0;
        $scope.commands = [];

        var experimentId = $routeParams.id;

        ExperimentService.get($routeParams.id).then(function(data) {
            $scope.experiment = data.data;
        }, function() {
            $location.path('/');
        });
        $scope.preview = {
            labels : [],
            series : [],
            data : [],
            options : {}
        };

        init();

        var previewChart = null;

        function init() {
            ExperimentService.getCommands($routeParams.id).then(
                    function(data) {
                        $scope.commands = data.data;

                        // close the accordion of each command
                        _.each($scope.commands, function(cmd) {
                            cmd._isopen = false;
                        });
                        prepareExecutionsForDisplay(_.flatten(_.pluck(
                                $scope.commands, 'executions')));
                    }, function() {
                        $location.path('/');
                    });
        }

        function updatePreviewChart() {

            var allExecutions = _.flatten(_
                    .pluck($scope.commands, 'executions'), true);

            if (previewChart == null) {
                previewChart = PreviewService.initChart(
                        'previewChartContainer', allExecutions);
            } else {
                previewChart.update(allExecutions);
            }
        }
        var lastCommandUpdate = new Date(0);
        var TIMER_INTERVAL = 3000;
        function scheduleUpdateCommandExecutions($scope) {
            var timer = $timeout(updateCommandExecutions, TIMER_INTERVAL);

            if ($scope != null) {
                $scope.$on("$destroy", function(event) {
                    $timeout.cancel(timer);
                });
            }
            return timer;
        }

        function updateCommandExecutions() {
            // Query all executions
            ExperimentService
                    .getCommandExecutions(experimentId,
                            lastCommandUpdate.toUTCString())
                    .then(
                            function(data) {
                                lastCommandUpdate = new Date();
                                lastCommandUpdate.setTime(lastCommandUpdate
                                        .getTime()
                                        - (TIMER_INTERVAL * 2));

                                var allExecutions = data.data;
                                prepareExecutionsForDisplay(allExecutions);

                                // replace
                                var groupedExecutions = _.groupBy(
                                        allExecutions, 'command');
                                _.each($scope.commands, function(cmd) {
                                    cmd.executions = _.has(groupedExecutions,
                                            cmd.id) ? groupedExecutions[cmd.id]
                                            : [];
                                });

                                updatePreviewChart();

                                // reschedule the polling.
                                scheduleUpdateCommandExecutions($scope);
                            },
                            function(data) {
                                // not modified, ok
                                if (data.status == 304) {
                                    scheduleUpdateCommandExecutions($scope);
                                    return;
                                }

                                // error, do not reschedule!
                                Snackbar
                                        .error('retrieving the experiment\'s commands failed '
                                                + data.statusText);
                            });

        }

        function prepareExecutionsForDisplay(allExecutions) {
            var doPrepare = function(exe) {
                exe.translatedState = ExperimentService
                        .translateExecutionState(exe.command_task != null ? exe.command_task.state
                                : null);
                exe.executionTime = ExperimentService.getExecutionRuntime(exe);
            };
            _.each(allExecutions, doPrepare);
        }

        updateCommandExecutions();

        vm.del = function() {
            console.log("Deleting experiment");
            console.log($scope.experiment);
        };

        vm.startAll = function() {

            ExperimentService.startAll($scope.experiment).then(
                    function() {
                        Snackbar.show('Experiment Started');
                    },
                    function(data) {
                        Snackbar.error('Starting experiment failed '
                                + data.statusText);
                    });
        };

        vm.stopAll = function() {
            ExperimentService.stopAll($scope.experiment).then(
                    function() {
                        Snackbar.show('Experiment Stopped');
                    },
                    function(data) {
                        Snackbar.error('Stopping experiment failed '
                                + data.statusText);
                    });
        };

        vm.startCommand = function(command) {
            ExperimentService.startCommand(command.id).then(function() {
                Snackbar.show('Command started');
            }, function(data) {
                Snackbar.error('starting command failed ' + data.statusText);
            });
        };

        vm.stopCommand = function(command) {
            ExperimentService.stopCommand(command.id).then(function() {
                Snackbar.show('Command stopped');
            }, function(data) {
                Snackbar.error('stopping command failed ' + data.statusText);
            });
        };

        vm.startExecution = function(execution) {
            ExperimentService.startExecution(execution.id).then(function() {
                Snackbar.show('Execution started');
            }, function(data) {
                Snackbar.error('starting Execution failed ' + data.statusText);
            });
        };

        vm.stopExecution = function(execution) {
            ExperimentService.stopExecution(execution.id).then(function() {
                Snackbar.show('Execution stopped');
            }, function(data) {
                Snackbar.error('stopping Execution failed ' + data.statusText);
            });
        };

        var maxLength = 100;
        vm.prettyCommandArgs = function(command) {
            var argStr = command.arguments.join(' ');
            if (argStr.length > maxLength) {
                return argStr.substring(maxLength) + '...';
            }
            return argStr;
        };

        vm.removeCommand = function(command, $index) {
            if (isCommandExecuting(command)) {
                DialogService.confirm(
                        '<strong>You should not delete the command while it is running. '
                                + 'Stop it first.</strong> Continue anyway?')
                        .then(confirmDelete);
            } else {
                confirmDelete();
            }

            function confirmDelete() {
                var doDelete = function() {
                    ExperimentService.deleteCommand(command.id, $index).then(
                            function() {
                                // close the accordion
                                // so
                                // the user is not
                                // confused
                                // with another thing
                                // opened
                                // which he seemed
                                // to have just removed.
                                command._isopen = false;
                                $scope.commands.splice($index, 1);
                            },
                            function(data) {
                                Snackbar.error("deleting the command failed "
                                        + data.statusText);
                            });
                };
                DialogService.confirm(
                        "<strong>Are you sure to delete the command</strong>?")
                        .then(doDelete);
            }
        };

        var isCommandExecuting = function(command) {
            return _.some(command.executions, function(exe) {
                return exe.translatedState == 'RUNNING';
            });
        };

        vm.editCommand = function(command) {
            if (isCommandExecuting(command)) {
                DialogService.confirm(
                        '<strong>You should not edit the command while it is running. '
                                + 'Stop it first.</strong> Continue anyway?')
                        .then(doEdit);
            } else {
                doEdit();
            }

            function doEdit() {
                $location.path('commands/' + command.id + '/edit');
            }
        };

        /**
         * Convenience object mapping execution states to css classes.
         */
        var stateMapper = {
            'RUNNING' : {
                'btn' : 'primary',
                'row' : 'active'
            },
            'IDLE' : {
                'default' : 'default',
            },
            'WAITING' : {
                'default' : 'default',
            },

            'FAILURE' : {
                'default' : 'warning',
            },

            'STOPPED' : {
                'default' : 'info',
            },

            'SUCCESS' : {
                'default' : 'success',
            },
        };

        /**
         * Returns the css class for a table
         * <tr> for an execution state
         */
        vm.getRowClassForState = function(execution) {

            return stateMapper[execution.translatedState]['row']
                    || stateMapper[execution.translatedState]['default'];
        };

        /**
         * Returns a button's css class for an execution state.
         */
        vm.getButtonClassForState = function(execution) {
            return "btn-"
                    + (stateMapper[execution.translatedState]['btn'] || stateMapper[execution.translatedState]['default']);
        };

        /**
         * Shows a dialog presenting details to the execution.
         */
        vm.showStatusDetails = function(execution) {
            var traceback = execution.command_task != null ? execution.command_task.traceback
                    : "No Traceback";
            ngDialog.open({
                template : 'executionStatusDetailsTemplate',
                data : {
                    traceback : traceback,
                    status : execution.translatedState,
                    statusClass : vm.getRowClassForState(execution),
                },
            });
        };

        vm.configurePreview = function(command, execution) {
            ngDialog.open({
                template : 'updatePreviewParserTemplate',
                controller : 'ExperimentUpdateParserConfigController',
                className : 'ngdialog-theme-default ngdialog-wide',
                data : {
                    execution : execution,
                    command : command,
                }
            }).closePromise.then(function(data) {
                if (data.value) {
                    vm.runParser(execution);
                }
            });
        };

        vm.runParser = function(execution) {
            ExperimentService.runParser(execution.id);
        };
    }

    controllers.controller('ExperimentUpdateParserConfigController', [
            '$scope', 'ExperimentService', 'ngDialog',
            ExperimentUpdateParserConfigController ]);

    function ExperimentUpdateParserConfigController($scope, ExperimentService,
            ngDialog) {
        var exe = $scope.ngDialogData.execution;
        var cmd = $scope.ngDialogData.command;
        var mapping = exe.preview_mapping;
        var vm = this;
        initialize();

        function initialize() {
            $scope.previewer = exe.previewer;
            $scope.previewer_series = exe.previewer_series;
            $scope.xMapping = mapping.x;
            $scope.yMapping = mapping.y;
            $scope.seriesMapping = mapping.series;

            // triggers re-executing the preview when this dialog is closed
            $scope.previewAutoexecute = true;

            var previewValues = exe.preview_data
                    && exe.preview_data.status == 'success' ? exe.preview_data.data
                    : {
                        values : []
                    };
            $scope.existingValues = _.uniq(_.flatten(_.map(
                    previewValues.values, function(d) {
                        return _.keys(d);
                    }), true));

            if (!exe.preview_data) {
                $scope.parserOutput = '';
            } else if (exe.preview_data.status == 'success') {
                $scope.parserOutput = JSON.stringify(exe.preview_data.data,
                        null, "  ");
            } else {
                $scope.parserOutput = exe.preview_data.data;
            }
        }

        ExperimentService.getPreviewers().then(function(data) {
            $scope.previewers = data.data;
        }, function(data) {
            Snackbar.error("loading previewers failed " + data.statusText);
        });

        $scope.save = function() {
            ExperimentService.updatePreviewSettings(exe.id, {
                preview_mapping : {
                    x : $scope.xMapping,
                    y : $scope.yMapping,
                    series : $scope.seriesMapping,
                },
                previewer : $scope.previewer,
                previewer_series : $scope.previewer_series
            }).then(function() {
                exe.previewer = $scope.previewer;
                exe.previewer_series = $scope.series;
                exe.preview_mapping = {
                    x : $scope.xMapping,
                    y : $scope.yMapping,
                    series : $scope.seriesMapping
                };
                $scope.closeThisDialog($scope.previewAutoexecute);
            });

        };

        $scope.showPreviewerHelp = function() {
            ngDialog
                    .open({
                        template : 'static/templates/experiments/preview_help_template.html'
                    });
        };

    }

    controllers.controller('ExperimentManageController', [ '$scope',
            '$location', '$routeParams', '$timeout', 'ExperimentService',
            'Snackbar', 'FileUploader', 'ngDialog', 'UtilsService', '$cookies',
            'DialogService', ExperimentManageController ]);

    function ExperimentManageController($scope, $location, $routeParams,
            $timeout, ExperimentService, Snackbar, FileUploader, ngDialog,
            UtilsService, $cookies, DialogService) {
        var vm = this;
        $scope.experimentInput = [];
        $scope.commands = [];
        $scope.commandExecutions = [];

        /**
         * File Uploader-Handlers
         */
        $scope.uploader = new FileUploader({
            url : window.urls('experiments_inputupload', $routeParams.id),
            autoUpload : true,
            removeAfterUpload : true,
        });

        $scope.uploader.onBeforeUploadItem = function(item) {
            item.headers['X-CSRFTOKEN'] = $cookies.get('csrftoken');
        };
        $scope.uploader.onCompleteItem = function() {
            updateRepoInputList($routeParams.id);
        };

        /**
         * Initializes the controller by loading experiment and command data
         */
        function initialize() {
            updateExperimentData();
            ExperimentService.getCommands($routeParams.id).then(function(data) {
                $scope.commands = data.data;
            }, function() {
                $location.path('/');
            });
            updateRepoInputList($routeParams.id);
        }

        initialize();

        vm.editExperiment = function(experiment) {
            ExperimentService.editExperiment(experiment).then(
                    updateExperimentData);
        };
        function updateExperimentData() {
            ExperimentService.get($routeParams.id).then(function(data) {
                $scope.experiment = data.data;
            }, function() {
                $location.path('/');
            });
        }

        function updateRepoInputList(experimentId) {
            ExperimentService
                    .repoListInput(experimentId)
                    .then(
                            function(data) {
                                $scope.experimentInput = data.data;
                            },
                            function(data) {
                                Snackbar
                                        .error("Could not update experiment input list "
                                                + data.statusText);
                            });
        }

        vm.removeItem = function(name) {
            ExperimentService.repoRemoveInput($scope.experiment.id, name).then(
                    function() {
                        updateRepoInputList($routeParams.id);
                    },
                    function(data) {
                        Snackbar.error("removing input file failed"
                                + data.statusText);
                    });
        };

        vm.syncRepository = function(experiment) {
            ngDialog.open({
                template : 'syncRepositoryTemplate',
                controller : 'SyncRepositoryDialogController',
                data : {
                    experiment : experiment
                },
            });
        };

        vm.cloneExperiment = function(experiment) {
            DialogService.info('Cloning not implemented yet');
        };

        vm.deleteExperiment = function(experiment) {
            DialogService.info('Delete not implemented yet');
        };

    }

    controllers.controller('SyncRepositoryDialogController', [ '$scope',
            'ExperimentService', 'Snackbar', SyncRepositoryDialogController ]);

    function SyncRepositoryDialogController($scope, ExperimentService, Snackbar) {
        var exp = $scope.ngDialogData.experiment;

        $scope.save = function() {
            ExperimentService
                    .syncRepository(exp.id, $scope.message, false)
                    .then(
                            function() {
                                $scope.closeThisDialog(0);
                            },
                            function(data) {
                                Snackbar
                                        .error('Synchronizing the repository failed '
                                                + data.statusText)
                            });
        };
    }

    angular.module('modsched.experiments.service').factory(
            'CommandEditService', CommandEditServiceFactory);

    CommandEditServiceFactory.$inject = [ 'Snackbar', 'ExecutableService',
            'ExperimentService', 'DialogService' ];

    function CommandEditServiceFactory(Snackbar, ExecutableService,
            ExperimentService, DialogService) {

        var service = {

            initScope : function($scope) {
                $scope.newArgumentObject = null;
                $scope.newArgument = null;
                $scope.newArgumentValue = null;
                $scope.executables = [];
                $scope.commandExecutions = [];
                $scope.previewer = '';
                $scope.previewer_series = '';

                // when selecting a preconfigured executable,
                // this function will be invoked, causing the pre-configured
                // arguments
                // to update.
                $scope.setExecutable = function(executable) {
                    $scope.command.executable = executable.path;
                    $scope.selectedExecutable = executable;
                };
            },

            initExecutableWatcher : function($scope) {
                $scope.$watch('executable', function(newExe, oldExe) {
                    service.checkAddExecutableDefaultArguments(newExe, oldExe,
                            $scope);
                });

            },

            checkAddExecutableDefaultArguments : function(newExecutable,
                    oldExecutable, $scope) {
                if (oldExecutable == newExecutable) {
                    return;
                }

                // skip if there are no arguments
                // OR there are no default arguments
                if (newExecutable.argument_set.length == 0
                        || !_.some(newExecutable.argument_set, function(arg) {
                            return arg.isDefault;
                        })) {
                    return;
                }

                DialogService
                        .confirm(
                                "Would you like to add the executable's default parameters?")
                        .then(function() {
                            service.addExecutableDefaultArguments($scope);
                        });

            },
            addExecutableDefaultArguments : function($scope) {
                var exe = $scope.command.executable;
                if (exe == null) {
                    return;
                }

                _.each(exe.argument_set, function(arg) {
                    if (!arg.isDefault) {
                        return;
                    }

                    var value = arg.choices.length > 0 ? ' ' + arg.choices[0]
                            : '';
                    service.addArgument($scope.command, arg.name, value);
                });
            },
            updateExecutableList : function($scope) {
                return ExecutableService
                        .all()
                        .then(
                                function(data) {
                                    $scope.executables = data.data;
                                },
                                function(data) {
                                    Snackbar
                                            .error("Could not load List of executables ("
                                                    + data.statusText + ")");
                                });
            },

            updatePreviewerList : function($scope) {
                return ExperimentService
                        .getPreviewers()
                        .then(
                                function(data) {
                                    $scope.previewers = data.data;
                                },
                                function(data) {
                                    Snackbar
                                            .error("Could not load list of previewers ("
                                                    + data.statusText + ")");
                                });
            },

            addArgument : function(command, newArgument, newArgumentValue) {
                if (newArgument == null) {
                    return;
                }

                var name = newArgument || '';
                var value = newArgumentValue || '';
                var combined = [ name, value ].join(' ').trim();

                /** empty string, ignore */
                if (!combined.length) {
                    return;
                }

                command.arguments.push(combined);
            },

            removeArgument : function(command, argument) {
                var argIndex = command.arguments.indexOf(argument);
                command.arguments.splice(argIndex, 1);
            },

            getExecutableById : function($scope, exeId) {
                return _.find($scope.executables, function(exe) {
                    return exe.id == exeId;
                });
            },

            previewCommands : function($scope, command) {

                // expand is disabled, don't talk to server
                if (!command.auto_expand) {
                    var cmd = [ command.executable ].concat(command.arguments);
                    $scope.commandExecutions = [ cmd.join(' ') ];
                    return;
                }

                return ExperimentService
                        .makeCommands(command.experiment, command.executable,
                                command.arguments)
                        .then(
                                function(data) {
                                    $scope.commandExecutions = data.data;
                                },
                                function(data) {
                                    $scope.commandExecutions = [ 'Error building the commands' ];
                                });
            },
        };

        return service;
    }

    controllers.controller('CommandCreateController', [ '$scope', '$location',
            '$routeParams', 'ExperimentService', 'CommandEditService',
            'Snackbar', CommandCreateController ]);

    function CommandCreateController($scope, $location, $routeParams,
            ExperimentService, CommandEditService, Snackbar) {
        var vm = this;
        initialize();

        $scope.submitButtonLabel = 'Create Command';
        $scope.action = 'add';
        function initialize() {
            CommandEditService.initScope($scope);
            CommandEditService.updateExecutableList($scope);
            CommandEditService.updatePreviewerList($scope);

            $scope.command = {
                executable : null,
                experiment : $routeParams.experimentId,
                arguments : [],
                auto_expand : true,
            };

            CommandEditService.initExecutableWatcher($scope);
        }

        vm.addArgument = function() {
            CommandEditService.addArgument($scope.command, $scope.newArgument,
                    $scope.newArgumentValue);
        };

        vm.removeArgument = function(arg) {
            CommandEditService.removeArgument($scope.command, arg);
        };

        vm.isArgumentEdited = function(index) {
            return $scope.editedArgument == index;
        };

        vm.editArgument = function(index) {
            if (index == $scope.editedArgument) {
                vm.resetArgumentEdit();
            } else {
                $scope.editedArgument = index;
            }
        };
        vm.resetArgumentEdit = function() {
            $scope.editedArgument = null;
        };

        vm.previewCommands = function() {
            CommandEditService.previewCommands($scope, $scope.command);
        };

        vm.submitCommand = function() {

            var command = {
                executable : $scope.command.executable,
                experiment : $routeParams.experimentId,
                arguments_expand : $scope.command.auto_expand,
                arguments : $scope.command.arguments,
                previewer : $scope.command.previewer,
                previewer_series : $scope.command.previewer_series,
            };
            var experimentId = $routeParams.experimentId;
            ExperimentService
                    .createCommand(experimentId, command)
                    .then(
                            function(data) {
                                $location.path('experiments/' + experimentId
                                        + '/show');
                            },
                            function(err) {
                                Snackbar
                                        .error('Error creating the command. See the log');
                            });
        };

    }

    controllers.controller('CommandEditController', [ '$scope', '$location',
            '$routeParams', 'ExperimentService', 'CommandEditService',
            'Snackbar', '$http', CommandEditController ]);

    function CommandEditController($scope, $location, $routeParams,
            ExperimentService, CommandEditService, Snackbar, $http) {
        var vm = this;
        initialize();

        $scope.submitButtonLabel = 'Save Changes';
        $scope.action = 'edit';

        function initialize() {
            $scope.editedArgument = null;

            CommandEditService.initScope($scope);
            CommandEditService.updatePreviewerList($scope);
            CommandEditService.updateExecutableList($scope).then(function() {
                loadCommandForEdit().then(function() {
                    CommandEditService.initExecutableWatcher($scope)
                });
            });
        }

        function loadCommandForEdit() {
            return ExperimentService.getCommandDetails($routeParams.commandId)
                    .then(function(data) {
                        var cmdData = data.data;
                        $scope.experimentId = cmdData.experiment;
                        $scope.command = {
                            id : cmdData.id,
                            executable : cmdData.executable,
                            experiment : cmdData.experiment,
                            arguments : cmdData.arguments,
                            auto_expand : cmdData.arguments_expand,
                            previewer : cmdData.previewer,
                            previewer_series : cmdData.previewer_series,
                        };

                        // check if an executable had been used, and set that as
                        // selected so the arguemnts get initialized.
                        _.each($scope.executables, function(predefExe) {
                            if (predefExe.path == $scope.command.executable) {
                                $scope.setExecutable(predefExe);
                            }
                        });

                    }, function(data) {
                        Snackbar.error(data.statusText);
                    });
        }
        vm.addArgument = function() {
            CommandEditService.addArgument($scope.command, $scope.newArgument,
                    $scope.newArgumentValue);
        };

        vm.isArgumentEdited = function(index) {
            return $scope.editedArgument == index;
        };

        vm.editArgument = function(index) {
            if (index == $scope.editedArgument) {
                vm.resetArgumentEdit();
            } else {
                $scope.editedArgument = index;
            }
        };
        vm.resetArgumentEdit = function() {
            $scope.editedArgument = null;
        };

        vm.removeArgument = function(arg) {
            CommandEditService.removeArgument($scope.command, arg);
            vm.resetArgumentEdit();
        };

        vm.previewCommands = function() {
            CommandEditService.previewCommands($scope, $scope.command);
        };

        vm.submitCommand = function() {

            var command = {
                executable : $scope.command.executable,
                experiment : $scope.command.experiment,
                arguments_expand : $scope.command.auto_expand,
                arguments : $scope.command.arguments,
                previewer : $scope.command.previewer,
                previewer_series : $scope.command.previewer_series,
            };
            ExperimentService
                    .updateCommand($scope.command.id, command)
                    .then(
                            function(data) {
                                $location.path('experiments/'
                                        + $scope.experimentId + '/show');
                            },
                            function(err) {
                                Snackbar
                                        .error('Error creating the command. See the log');
                            });
        };

    }

})();
