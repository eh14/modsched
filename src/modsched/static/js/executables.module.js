(function() {
    'use strict';

    angular.module('modsched.executables',
            [ 'modsched.executables.controllers',
                    'modsched.executables.service' ]);

    angular.module('modsched.executables.controllers', []);

    angular.module('modsched.executables.service', []);

    /** *** Service **** */
    angular.module('modsched.executables.service').factory('ExecutableService',
            serviceFactory);

    serviceFactory.$inject = [ '$http' ];

    function serviceFactory($http) {
        return {
            all : function() {
                return $http.get(window.urls('executables_list'));
            },
            create : function(experiment) {
                return $http.post(window.urls('executables_list'), experiment);
            },
            get : function(id) {
                return $http.get(window.urls('executables_detail', id));
            },
            update : function(experiment) {
                return $http.put(window.urls('executables_detail',
                        experiment.id), experiment);
            },
            deleteArgument : function(propId) {
                return $http['delete'](window.urls('arguments_detail', propId));
            },
            addArgument : function(argument) {
                var argument = angular.copy(argument);
                argument.choices = argument.choices.split(',');
                return $http.post(window.urls('arguments_list'), argument);
            },
            updateArgument : function(argument) {
                var argument = angular.copy(argument);
                argument.choices = argument.choices.split(',');
                return $http.put(window.urls('arguments_detail', argument.id),
                        argument);
            },
            addExecutable : function(executable) {
                return $http.post(window.urls('executables_list'), executable);
            },
            updateExecutable : function(executable) {
                // don't submit the arguments
                var stripped = angular.copy(executable);
                stripped.argument_set = [];
                return $http.put(window.urls('executables_detail',
                        executable.id), stripped);
            },
            deleteExecutable : function(executableId) {
                return $http['delete'](window.urls('executables_detail',
                        executableId));
            },
        };
    }

    /** *** Controllers **** */
    var controllers = angular.module('modsched.executables.controllers');

    controllers
            .controller(
                    'ExecutableListController',
                    [
                            '$scope',
                            'ExecutableService',
                            'Snackbar',
                            'ngDialog',
                            'DialogService',
                            function($scope, ExecutableService, Snackbar,
                                    ngDialog, DialogService) {
                                var vm = this;

                                $scope.accordions_open = {};

                                vm.removeArgument = function(propId) {
                                    DialogService
                                            .confirm(
                                                    "Are you sure to delete the argument?")
                                            .then(
                                                    function() {
                                                        ExecutableService
                                                                .deleteArgument(
                                                                        propId)
                                                                .then(
                                                                        reloadExecutables,
                                                                        function(
                                                                                data) {
                                                                            Snackbar
                                                                                    .error("removing argument failed"
                                                                                            + data.statusText);
                                                                        });
                                                    });
                                };

                                vm.addArgument = function(executableId) {
                                    ngDialog.open({
                                        template : 'editArgumentTemplate',
                                        controller : 'ArgumentAddController',
                                        data : {
                                            argument : {
                                                executable : executableId
                                            }
                                        },
                                    }).closePromise.then(function(data) {
                                        if (data.value) {
                                            reloadExecutables();
                                        }
                                    });
                                };

                                vm.editArgument = function(argument) {
                                    ngDialog.open({
                                        template : 'editArgumentTemplate',
                                        controller : 'ArgumentEditController',
                                        data : {
                                            argument : argument,
                                        },
                                    }).closePromise.then(function(data) {
                                        if (data.value) {
                                            reloadExecutables();
                                        }
                                    });
                                };

                                vm.addExecutable = function() {
                                    ngDialog.open({
                                        template : 'editExecutableTemplate',
                                        controller : 'ExecutableAddController',
                                    }).closePromise.then(function(data) {
                                        if (data.value) {
                                            reloadExecutables();
                                        }
                                    });
                                };
                                vm.editExecutable = function(executable) {
                                    ngDialog
                                            .open({
                                                template : 'editExecutableTemplate',
                                                controller : 'ExecutableEditController',
                                                data : {
                                                    executable : executable
                                                },
                                            }).closePromise
                                            .then(function(data) {
                                                if (data.value) {
                                                    reloadExecutables();
                                                }
                                            });
                                };

                                vm.removeExecutable = function(executable) {
                                    DialogService
                                            .confirm(
                                                    "Are you sure to delete the executable <strong>"
                                                            + executable.path
                                                            + "</strong>?")
                                            .then(
                                                    function() {
                                                        ExecutableService
                                                                .deleteExecutable(
                                                                        executable.id)
                                                                .then(
                                                                        reloadExecutables,
                                                                        function(
                                                                                data) {
                                                                            Snackbar
                                                                                    .error("deleting Executable failed "
                                                                                            + data.statusText);
                                                                        });
                                                    });
                                };

                                $scope.executables = [];
                                reloadExecutables();

                                function reloadExecutables() {
                                    ExecutableService
                                            .all()
                                            .then(
                                                    function(data, status,
                                                            headers, config) {
                                                        $scope.executables = data.data;
                                                    },
                                                    function(data, status,
                                                            headers, config) {
                                                        Snackbar
                                                                .error("reloading Executables failed"
                                                                        + data.statusText);
                                                    });
                                }

                                vm.join = function(value) {
                                    return value.join();
                                };

                            } ]);

    controllers.controller('ArgumentAddController', [
            '$scope',
            'ExecutableService',
            'Snackbar',
            function($scope, ExecutableService, Snackbar) {
                $scope.argument = $scope.ngDialogData.argument;
                $scope.action = 'Add';
                var vm = this;
                $scope.save = function() {
                    ExecutableService.addArgument($scope.argument).then(
                            function() {
                                $scope.closeThisDialog(true);
                            },
                            function(data) {
                                Snackbar.error("adding the argument failed "
                                        + data.statusText);
                            });
                };
            } ]);

    controllers.controller('ArgumentEditController', [
            '$scope',
            'ExecutableService',
            'Snackbar',
            function($scope, ExecutableService, Snackbar) {
                $scope.argument = angular.copy($scope.ngDialogData.argument);
                $scope.argument.choices = $scope.argument.choices.join(',');
                $scope.action = 'Update';
                var vm = this;
                $scope.save = function() {
                    ExecutableService.updateArgument($scope.argument).then(
                            function() {
                                $scope.closeThisDialog(true);
                            },
                            function(data) {
                                Snackbar.error("updating the argument failed "
                                        + data.statusText);
                            });
                };
            } ]);

    controllers.controller('ExecutableAddController', [
            '$scope',
            'ExecutableService',
            'Snackbar',
            function($scope, ExecutableService, Snackbar) {
                $scope.executable = {};
                $scope.action = 'Add';
                var vm = this;
                $scope.save = function() {
                    ExecutableService.addExecutable($scope.executable).then(
                            function() {
                                $scope.closeThisDialog(true);
                            },
                            function(data) {
                                $scope.error = "Error adding Executable";
                            });
                };
            } ]);

    controllers.controller('ExecutableEditController', [
            '$scope',
            'ExecutableService',
            'Snackbar',
            function($scope, ExecutableService, Snackbar) {
                $scope.executable = angular
                        .copy($scope.ngDialogData.executable);
                $scope.action = 'Edit';
                var vm = this;
                $scope.save = function() {
                    ExecutableService.updateExecutable($scope.executable).then(
                            function() {
                                $scope.closeThisDialog(true);
                            },
                            function(data) {
                                $scope.error = "Error updating Executable";
                            });
                };
            } ]);
})();
