(function() {
    'use strict';

    angular.module('modsched.layout.controllers').controller(
            'NavbarController', NavbarController);

    NavbarController.$inject = [ '$scope', 'Authentication',
            'ExperimentService' ];

    /**
     * @namespace NavbarController
     */
    function NavbarController($scope, Authentication, ExperimentService) {
        var vm = this;

        vm.logout = logout;

        /**
         * @name logout
         * @desc Log the user out
         * @memberOf thinkster.layout.controllers.NavbarController
         */
        function logout() {
            Authentication.logout();
        }

        vm.createExperiment = function() {
            ExperimentService.addExperiment();
        };
    }
})();