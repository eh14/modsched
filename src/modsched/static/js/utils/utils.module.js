(function() {
    'use strict';

    angular.module('modsched.utils', [ 'modsched.utils.service', ]);

    angular.module('modsched.utils.service', []);
})();