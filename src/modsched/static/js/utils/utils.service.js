(function($, _) {
    'use strict';

    angular.module('modsched.utils.service').factory('Snackbar', Snackbar);

    function Snackbar() {
        var Snackbar = {
            error : error,
            show : show
        };

        return Snackbar;
        function _snackbar(content, options) {
            options = _.extend({
                timeout : 3000
            }, options);
            options.content = content;

            $.snackbar(options);
        }

        function error(content, options) {
            _snackbar('Error: ' + content, options);
        }

        function show(content, options) {
            _snackbar(content, options);
        }
    }

    angular.module('modsched.utils.service').factory('DialogService',
            [ 'ngDialog', DialogService ]);

    function DialogService(ngDialog) {

        return {
            confirm : function(text) {

                return ngDialog
                        .openConfirm({
                            template : '<h3>Confirmation required.</h3><br /><p>'
                                    + text
                                    + '</p><div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(1)">Yes</button>\
                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
                     </div>',
                            closeByEscape : false,
                            showClose : false,
                            plain : true,
                        });
            },

            info : function(text) {
                ngDialog
                        .open({
                            template : '<p>'
                                    + text
                                    + '</p>'
                                    + '<p><div class="ngdialog-buttons"><button type="button" class="ngdialog-button ngdialog-button-secondary" \
                            ng-click="closeThisDialog(0)">Ok</button></p></div>',
                            closeByEscape : true,
                            plain : true,
                        });
            }
        }
    }

    angular.module('modsched.utils.service').factory('UtilsService',
            function() {
                return {
                    removeFromArray : function(list, element) {
                        var cmdIndex = list.indexOf(element);
                        if (cmdIndex > -1) {
                            list.splice(element, 1);
                        }
                    }
                }
            });
})($, _);