(function() {
    'use strict';

    angular.module('modsched.preview', [ 'modsched.preview.service', ]);

    angular.module('modsched.preview.service', []);

    angular.module('modsched.preview.service').factory('PreviewService',
            PreviewService);

    var Chart = function(elementId) {

        var chartOptions = {
            chart : {
                type : 'line',
                // animation : Highcharts.svg, // don't
                // animate
                // in old IE
                marginRight : 10,
            },
            title : {
                text : null,
            },
            xAxis : {},
            yAxis : {
                title : {
                    text : 'Value'
                },
            },
            tooltip : {
                formatter : function() {
                    var props = _.map(_.pairs(this.point.arguments),
                            function(pair) {
                                if (pair[1]) {
                                    return pair[0] + ': ' + pair[1] + '<br >';
                                } else {
                                    return '';
                                }
                            }).join('');
                    return '<b>' + this.series.name + '</b><br/>' + props;
                }
            },
            legend : {
                enabled : true
            },
            series : []
        };
        $('#' + elementId).highcharts(chartOptions);

        var chart = $('#' + elementId).highcharts();

        function update(executions) {

            var seriesData = {};
            var addToSeries = function(series, element) {
                seriesData[series] = seriesData[series] || [];

                seriesData[series].push(element);
            };

            //
            // iterate over all executions, ignore them if they don't have
            // preview_data or the preview failed.
            // Then for each value in the data-part of the preview_data,
            // create a point using the configured mapping from the execution,
            // or the default one.
            // If the points do not provide series, use the execution's series.
            _.each(executions, function(element) {
                if (element.preview_data == null
                        || element.preview_data.status != 'success') {
                    return;
                }

                var executionSeries = element.previewer_series;
                var mapping = element.preview_mapping;
                var xMapping = mapping['x'] || 'x';
                var yMapping = mapping['y'] || 'y';
                var seriesMapping = mapping['series'] || 'series';

                _.each(element.preview_data.data.values, function(value) {
                    var series = value[seriesMapping] || executionSeries;
                    var point = {
                        x : value[xMapping],
                        y : value[yMapping],
                        arguments : value,
                    };
                    addToSeries(series, point);
                });

            });

            // get lists of all existing series in the chart
            // and the ones of our points.
            var existingSeries = _.pluck(chart.series, 'name');
            var newSeries = _.keys(seriesData);

            // create those that are not present
            _.each(_.difference(newSeries, existingSeries), function(toCreate) {
                chart.addSeries({
                    name : toCreate,
                    id : toCreate,
                    data : [],
                });
            });

            // and remove the unneeded
            _.each(_.difference(existingSeries, newSeries), function(toRemove) {
                chart.get(toRemove).remove();
            });

            // replace all points in all series
            _.each(seriesData, function(points, serie) {
                // sort the points so the line is connected correctly
                points.sort(function(left, right) {
                    return left.x - right.x;
                });
                chart.get(serie).setData(points, false);
            });

            // redraw the chart
            chart.redraw();
        }

        return {
            update : update
        };
    }

    function PreviewService() {
        return {
            initChart : function(elementId, initialExecutions) {
                return Chart(elementId, initialExecutions);
            },
        };
    }
})();