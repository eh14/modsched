(function() {
    'use strict';

    angular.module('modsched.config', [ 'modsched.config.service' ]).config(
            config);

    config.$inject = [ '$locationProvider', '$httpProvider' ];

    /**
     * @name config
     * @desc Enable HTML5 routing
     */
    function config($locationProvider, $httpProvider) {
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

        $httpProvider.interceptors.push('ResponseObserver');
    }

    angular.module('modsched.config.service', [ 'modsched.utils.service' ])
            .factory('ResponseObserver',
                    [ '$q', '$location', '$injector', ResponseObserver ]);

    function ResponseObserver($q, $location, $injector) {
        return {
            'responseError' : function(rejection) {
                if (rejection.status == 403) {
                    var DialogService = $injector.get('DialogService');
                    DialogService
                            .info('You do not have permission to perform this kind of action!');
                }
                return $q.reject(rejection);
            }
        };
    }

})();