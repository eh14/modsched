(function() {
    'use strict';

    var routeConfig = function($routeProvider) {
        $routeProvider.when('/login', {
            controller : 'LoginController',
            controllerAs : 'vm',
            templateUrl : '/static/templates/authentication/login.html'
        }).when('/', {
            controller : 'DashboardController',
            controllerAs : 'vm',
            templateUrl : '/static/templates/dashboard.html'
        });

        // Experiment Routes
        $routeProvider.when('/experiments', {
            controller : 'ExperimentListController',
            controllerAs : 'vm',
            templateUrl : '/static/templates/experiments/list.html'
        }).when('/experiments/:id/show', {
            controller : 'ExperimentShowController',
            controllerAs : 'vm',
            templateUrl : '/static/templates/experiments/show.html'
        }).when('/experiments/:id/edit', {
            controller : 'ExperimentManageController',
            controllerAs : 'vm',
            templateUrl : '/static/templates/experiments/manage.html'
        });

        // Command Routes
        $routeProvider.when('/commands/:commandId/edit', {
            controller : 'CommandEditController',
            controllerAs : 'vm',
            templateUrl : '/static/templates/experiments/edit_command.html'
        }).when('/commands/:experimentId/create', {
            controller : 'CommandCreateController',
            controllerAs : 'vm',
            templateUrl : '/static/templates/experiments/edit_command.html'
        });

        // Executable Routes
        $routeProvider.when('/executables', {
            controller : 'ExecutableListController',
            controllerAs : 'vm',
            templateUrl : '/static/templates/executables/list.html'
        });

        // if nothing else matches, go to root.
        $routeProvider.otherwise('/');
    }
    routeConfig.$inject = [ '$routeProvider' ];

    angular.module('modsched.routes').config(routeConfig);

})();