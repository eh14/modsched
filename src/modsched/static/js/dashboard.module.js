(function() {
    'use strict';

    angular.module('modsched.dashboard', [ 'modsched.dashboard.controllers',
            'modsched.dashboard.directives', 'modsched.dashboard.service' ]);

    angular.module('modsched.dashboard.controllers', []);

    angular.module('modsched.dashboard.directives', []);

    angular.module('modsched.dashboard.service', []);

    var controllers = angular.module('modsched.dashboard.controllers');

    /** *** Service **** */
    angular.module('modsched.dashboard.service').factory('DashboardService',
            DashboardService);

    DashboardService.$inject = [ '$http', '$location', 'ngDialog', 'Snackbar' ];

    function DashboardService($http, $location, ngDialog, Snackbar) {
        return {
            loadInitial : function() {
                return $http.get(window.urls('dashboard_initial'));
            },
            loadExecutionState : function(lastUpdated) {
                return $http.get(window.urls('dashboard_state'), {
                    headers : {
                        "if-modified-since" : lastUpdated
                    }
                });
            },
        };
    }

    controllers.controller('DashboardController', [ '$scope', '$timeout',
            'Authentication', '$routeParams', 'DashboardService',
            'ExperimentService', 'Snackbar', 'DialogService', 'ngDialog',
            DashboardController ]);

    function DashboardController($scope, $timeout, Authentication,
            $routeParams, DashboardService, ExperimentService, Snackbar,
            DialogService, ngDialog, PreviewService) {
        var vm = this;

        // this model field, set by the checkbox cannot go to the scope
        // since child-scopes wouldn't change with the parent.
        vm.showInactive = false;
        vm.isAuthenticated = Authentication.isAuthenticated();

        activate();

        function activate() {
            $scope.experiments = [];
            $scope.totalNumExecutions = 0;
            $scope.experimentMap = {};

            DashboardService.loadInitial().then(function(data) {
                $scope.experiments = _.map(data.data, function(expData) {
                    var exp = initializeExperiment(expData);
                    updateExperimentState(exp);

                    return exp;
                });

                // create a second map to access experiments via ID. This is
                // much easier for updates, especially whiel they're moving
                // between active and idleExperiments-collections
                $scope.experimentMap = _.indexBy($scope.experiments, 'id');

                makeExperimentsStatistics();

                scheduleUpdateExecutions();
            });
        }

        function initializeExperiment(expData) {
            var mapInitialExecution = function(exe) {
                return {
                    id : exe.id,
                    state : exe.command_task != null ? exe.command_task.state
                            : 'PENDING',
                    task_id : exe.command_task_id,
                    tstamp : exe.command_task != null ? exe.command_task.tstamp
                            : null
                };
            };
            var executions = _.indexBy(_.flatten(_.map(expData.command_set,
                    function(cmd) {
                        return _.map(cmd.executions, mapInitialExecution);
                    })), 'id');

            return {
                id : expData.id,
                title : expData.title,
                creator : {
                    username : expData.creator.username,
                    email : expData.creator.email
                },
                executions : executions,
                numExecutions : _.keys(executions).length,
                states : {},
                isActive : false,
            };
        }

        function updateExperimentState(experiment) {
            var exeStates = _.map(_.values(experiment.executions),
                    function(exe) {
                        return ExperimentService
                                .translateExecutionState(exe.state);
                    });

            var stateGroups = _.groupBy(exeStates);
            experiment.states = stateGroups;
            experiment.isActive = _.has(stateGroups, 'RUNNING')
                    || _.has(stateGroups, 'WAITING');
        }

        function checkUpdateExperiment(exeUpdate) {
            if (!_.has($scope.experimentMap, exeUpdate.id)) {
                console
                        .log('Warn: new experiment that is not in our list. You should reload. Or should we?');
                return;
            }

            updateExperiment($scope.experimentMap[exeUpdate.id], exeUpdate)
        }

        function updateExperiment(experiment, newData) {

            function updateExecution(newExe) {
                if (!_.has(experiment.executions, newExe.id)) {
                    console
                            .log('Warn: new execution ID that is not in our list. '
                                    + 'You should reload the page. Or should we?');
                    return;
                }

                var exe = experiment.executions[newExe.id];
                exe.state = newExe.state;
                exe.tstamp = newExe.tstamp;
            }

            _.each(newData.executions, updateExecution);

            updateExperimentState(experiment);
        }

        var lastUpdate = new Date(0);
        var TIMER_INTERVAL = 3000;

        function scheduleUpdateExecutions() {
            var timer = $timeout(executionStatePoller, TIMER_INTERVAL);

            $scope.$on("$destroy", function(event) {
                $timeout.cancel(timer);
            });
            return timer;
        }

        function executionStatePoller(executionState) {
            DashboardService.loadExecutionState(lastUpdate.toUTCString()).then(
                    function(data) {
                        lastUpdate = new Date();
                        lastUpdate.setTime(lastUpdate.getTime()
                                - (TIMER_INTERVAL * 2));

                        _.each(data.data, function(expUpdate) {
                            checkUpdateExperiment(expUpdate);
                        });

                        scheduleUpdateExecutions();
                    },
                    function(data) {
                        Snackbar.error('updating execution states '
                                + data.statusText);
                    });
        }

        function makeExperimentsStatistics() {
            $scope.totalNumExecutions = _.reduce($scope.experiments, function(
                    memo, exp) {
                return exp.numExecutions + memo;
            }, 0);
        }

        $scope.showExperiment = function(exp) {
            return exp.isActive || vm.showInactive;
        };

        vm.createExperiment = function() {
            ExperimentService.addExperiment();
        };

    }
})();