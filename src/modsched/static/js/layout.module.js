(function() {
    'use strict';

    angular.module('modsched.layout', [ 'modsched.layout.controllers' ]);

    angular.module('modsched.layout.controllers', []);

})();