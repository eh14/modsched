from settings import *

# override database settings so we can work faster.

DATABASES['default']['NAME'] = 'itest_modsched'
DATABASES['default']['TEST'] = {'NAME':'itest_modsched'}
