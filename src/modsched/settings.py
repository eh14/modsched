"""
Django settings for modsched project.

Generated by 'django-admin startproject' using Django 1.8.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from modsched.execution import tasks

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEPLOY_DIR = os.path.abspath(os.path.join(BASE_DIR, '..', 'deploy'))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'b!xuk-m(n9-$b()dn&#ar8-b3vme+@hm@l7bx+terr$t8curx#'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = (

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'compressor',
    'modsched',
    'authentication',
    'djangobower',
    'rest_framework',
    'djcelery',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'modsched.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'modsched.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'USER':'root',
        'PASSWORD':'root',
        'NAME':'modsched',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(DEPLOY_DIR, 'static')


STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'djangobower.finders.BowerFinder',
    'compressor.finders.CompressorFinder',
)

BOWER_COMPONENTS_ROOT = os.path.join(BASE_DIR, 'modsched/static/')

BOWER_INSTALLED_APPS = (
    'jquery#2.1.3',
    'bootstrap#3.3.4',
    'jasny-bootstrap#3.1.3',
    'angular#1.4.0',
    'ngDialog#0.3.12',
    'snackbarjs#1.0.0',
    'underscore#1.8.3',
    'bootbox#4.4.0',
    'angular-file-upload#1.1.5',
    'highcharts-release',
    'ng-busy',
)

### celery config ###
from kombu import Exchange, Queue

BROKER_URL = 'amqp://guest@localhost/'

CELERY_RESULT_BACKEND = 'amqp'
CELERY_TASK_RESULT_EXPIRES = 1800
# CELERY_MONGODB_BACKEND_SETTINGS = {
#     'database': 'modsched',
#     'taskmeta_collection': 'results',
# }

CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

CELERY_ACCEPT_CONTENT = ['json']


CELERY_CREATE_MISSING_QUEUES = True

CELERY_ROUTES = (tasks.TaskRouter(),)
CELERY_QUEUES = (
    Queue('commands', Exchange('default'), routing_key='commands'),
    Queue('sidejobs', Exchange('default'), routing_key='sidejobs'),
)

# CELERY_TASK_RESULT_EXPIRES = 60
CELERY_TRACK_STARTED = True
CELERY_SEND_TASK_SENT_EVENT = True
CELERY_SEND_EVENTS = True

######################################
# Application specific settings
######################################

MODSCHED_PROJECTDIR = os.path.abspath(os.path.join(BASE_DIR, '..', '_workingdir_'))


LOGGING = {
    'disable_existing_loggers':False,
    'version':1,
    'handlers': {
        'console': {
            # logging handler that outputs log messages to terminal
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',  # message level to be written to console
        },
    },
    'loggers': {
        '': {
            # this sets root level logger to log debug and higher level
            # logs to console. All other loggers inherit settings from
            # root level logger.
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,  # this tells logger to send logging message
                                # to its parent (will send if set to True)
        },
        'django.db': {
            # django also has database level logging
            'level':'INFO',
        },
    },
}


REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticatedOrReadOnly',
    )
}
