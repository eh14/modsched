'''

@author: eh14
'''
import time


def waitForTask(asyncResult, timeout, initial='PENDING'):
    """
    Waits for a task to leave the state PENDING.
    The state new state is returned. After a timeout,
    an assertion is raised.
    """
    assert 0 <= timeout <= 20, "timeout must be between 0 and 20."
    end = time.time() + timeout
    while True:
        if asyncResult.status != initial:
            return asyncResult.status
        
        if time.time() > end:
            raise AssertionError('Timeout waiting for task')
        
        time.sleep(0.1)
