PRISM
=====

Version: 4.2.beta1
Date: Wed Mar 04 16:21:54 CET 2015
Hostname: obiwan
Command line: prism qstoc20.prism props.csl -const 'error=0.5,loss=0.1' -explicit -exportresults ./results/20_results.txt -sbmax 10240 -gsmax 10240 -cuddmaxmem 10485760 -maxiters 9999999 -valiter -epsilon 1e-5

Parsing model file "qstoc20.prism"...

Parsing properties file "props.csl"...

8 properties:
(1) P=? [ F<=10 ps=2 ]
(2) P=? [ G ((ps=7|ps=5)=>P>0.9 [ F<=10 (ps=2) ]) ]
(3) P=? [ F<=10 (ps=2|state=terminal) ]
(4) P=? [ G ((ps=7|ps=5)=>P>0.9 [ F<=10 (ps=2|state=terminal) ]) ]
(5) P=? [ G ((ps=7|ps=5)=>(P>0.9 [ F<=10 (ps=2|state=terminal) ]|state=terminal)) ]
(6) P=? [ F<=10 (ps=2&state!=terminal) ]
(7) P=? [ G ((ps=7|ps=5)=>P>0.9 [ F<=10 (ps=2&state!=terminal) ]) ]
(8) P=? [ G ((ps=7|ps=5)=>(P>0.9 [ F<=10 (ps=2&state!=terminal) ]&state!=terminal)) ]

Type:        CTMC
Modules:     ts s r channel 
Variables:   state ss ps sr pr s6 s4 s1 

---------------------------------------------------------------------

Model checking: P=? [ F<=10 ps=2 ]
Model constants: error=0.5,loss=0.1

Building model...
Model constants: error=0.5,loss=0.1

Computing reachable states... 958 2093 3177 4149 5219 6276 7448 8565 9581 9676 states
Reachable states exploration and model construction done in 27.291 secs.
Sorting reachable states list...

Time for model construction: 27.363 seconds.

Warning: Deadlocks detected and fixed in 215 states

Type:        CTMC
States:      9676 (1 initial)
Transitions: 23383

Starting backwards transient probability computation...

Uniformisation: q.t = 3.672 x 10.0 = 36.72
Fox-Glynn (1.25E-6): left = 0, right = 70
Backwards transient probability computation took 71 iters and 0.111 seconds.

Value in the initial state: 0.8659141967219126

Time for model checking: 0.126 seconds.

Result: 0.8659141967219126 (value in the initial state)

---------------------------------------------------------------------

Model checking: P=? [ G ((ps=7|ps=5)=>P>0.9 [ F<=10 (ps=2) ]) ]
Model constants: error=0.5,loss=0.1

Starting backwards transient probability computation...

Uniformisation: q.t = 3.672 x 10.0 = 36.72
Fox-Glynn (1.25E-6): left = 0, right = 70
Backwards transient probability computation took 71 iters and 0.092 seconds.

Warning: Switching to linear equation solution method "Gauss-Seidel"

Starting probabilistic reachability...
Starting Prob0...
Prob0 took 23 iterations and 0.045 seconds.
Starting Prob1...
Prob1 took 36 iterations and 0.064 seconds.
target=191, yes=226, no=7723, maybe=1727
Starting Gauss-Seidel...
Gauss-Seidel took 685 iterations and 0.119 seconds.
Probabilistic reachability took 0.231 seconds.

Value in the initial state: -0.0867500000000001

Time for model checking: 0.338 seconds.

Result: -0.0867500000000001 (value in the initial state)

---------------------------------------------------------------------

Model checking: P=? [ F<=10 (ps=2|state=terminal) ]
Model constants: error=0.5,loss=0.1

Starting backwards transient probability computation...

Uniformisation: q.t = 3.672 x 10.0 = 36.72
Fox-Glynn (1.25E-6): left = 0, right = 70
Backwards transient probability computation took 71 iters and 0.028 seconds.

Value in the initial state: 0.8667223185486038

Time for model checking: 0.031 seconds.

Result: 0.8667223185486038 (value in the initial state)

---------------------------------------------------------------------

Model checking: P=? [ G ((ps=7|ps=5)=>P>0.9 [ F<=10 (ps=2|state=terminal) ]) ]
Model constants: error=0.5,loss=0.1

Starting backwards transient probability computation...

Uniformisation: q.t = 3.672 x 10.0 = 36.72
Fox-Glynn (1.25E-6): left = 0, right = 70
Backwards transient probability computation took 71 iters and 0.028 seconds.

Warning: Switching to linear equation solution method "Gauss-Seidel"

Starting probabilistic reachability...
Starting Prob0...
Prob0 took 9 iterations and 0.013 seconds.
Starting Prob1...
Prob1 took 21 iterations and 0.022 seconds.
target=94, yes=107, no=9278, maybe=291
Starting Gauss-Seidel...
Gauss-Seidel took 17 iterations and 0.001 seconds.
Probabilistic reachability took 0.038 seconds.

Value in the initial state: -0.0867500000000001

Time for model checking: 0.069 seconds.

Result: -0.0867500000000001 (value in the initial state)

---------------------------------------------------------------------

Model checking: P=? [ G ((ps=7|ps=5)=>(P>0.9 [ F<=10 (ps=2|state=terminal) ]|state=terminal)) ]
Model constants: error=0.5,loss=0.1

Starting backwards transient probability computation...

Uniformisation: q.t = 3.672 x 10.0 = 36.72
Fox-Glynn (1.25E-6): left = 0, right = 70
Backwards transient probability computation took 71 iters and 0.028 seconds.

Warning: Switching to linear equation solution method "Gauss-Seidel"

Starting probabilistic reachability...
Starting Prob0...
Prob0 took 9 iterations and 0.015 seconds.
Starting Prob1...
Prob1 took 21 iterations and 0.025 seconds.
target=94, yes=107, no=9278, maybe=291
Starting Gauss-Seidel...
Gauss-Seidel took 17 iterations and 0.0 seconds.
Probabilistic reachability took 0.041 seconds.

Value in the initial state: -0.0867500000000001

Time for model checking: 0.072 seconds.

Result: -0.0867500000000001 (value in the initial state)

---------------------------------------------------------------------

Model checking: P=? [ F<=10 (ps=2&state!=terminal) ]
Model constants: error=0.5,loss=0.1

Starting backwards transient probability computation...

Uniformisation: q.t = 3.672 x 10.0 = 36.72
Fox-Glynn (1.25E-6): left = 0, right = 70
Backwards transient probability computation took 71 iters and 0.072 seconds.

Value in the initial state: 0.8652163320671278

Time for model checking: 0.075 seconds.

Result: 0.8652163320671278 (value in the initial state)

---------------------------------------------------------------------

Model checking: P=? [ G ((ps=7|ps=5)=>P>0.9 [ F<=10 (ps=2&state!=terminal) ]) ]
Model constants: error=0.5,loss=0.1

Starting backwards transient probability computation...

Uniformisation: q.t = 3.672 x 10.0 = 36.72
Fox-Glynn (1.25E-6): left = 0, right = 70
Backwards transient probability computation took 71 iters and 0.07 seconds.

Warning: Switching to linear equation solution method "Gauss-Seidel"

Starting probabilistic reachability...
Starting Prob0...
Prob0 took 6 iterations and 0.003 seconds.
Starting Prob1...
Prob1 took 16 iterations and 0.01 seconds.
target=4555, yes=4807, no=2035, maybe=2834
Starting Gauss-Seidel...
Gauss-Seidel took 21 iterations and 0.007 seconds.
Probabilistic reachability took 0.02 seconds.

Value in the initial state: -0.016052500000000025

Time for model checking: 0.093 seconds.

Result: -0.016052500000000025 (value in the initial state)

---------------------------------------------------------------------

Model checking: P=? [ G ((ps=7|ps=5)=>(P>0.9 [ F<=10 (ps=2&state!=terminal) ]&state!=terminal)) ]
Model constants: error=0.5,loss=0.1

Starting backwards transient probability computation...

Uniformisation: q.t = 3.672 x 10.0 = 36.72
Fox-Glynn (1.25E-6): left = 0, right = 70
Backwards transient probability computation took 71 iters and 0.072 seconds.

Warning: Switching to linear equation solution method "Gauss-Seidel"

Starting probabilistic reachability...
Starting Prob0...
Prob0 took 6 iterations and 0.003 seconds.
Starting Prob1...
Prob1 took 16 iterations and 0.01 seconds.
target=4555, yes=4807, no=2035, maybe=2834
Starting Gauss-Seidel...
Gauss-Seidel took 21 iterations and 0.006 seconds.
Probabilistic reachability took 0.021 seconds.

Value in the initial state: -0.016052500000000025

Time for model checking: 0.095 seconds.

Result: -0.016052500000000025 (value in the initial state)

Exporting results to file "./results/20_results.txt"...

---------------------------------------------------------------------

Note: There were 6 warnings during computation.

