from django.test import TestCase
from modsched.execution import tasks
import time
from modsched.tests.creator import CreatorMixin
from modsched import models
from modsched.execution.parsers import LinePreviewResult
import os

class TestRunCommand(TestCase, CreatorMixin):
    
        
    def test_savePreviewResult_success(self):
        
        prevData = self._createPreviewData()
        seqId = prevData.sequence_id
        
        result = LinePreviewResult(sequence_id=seqId - 1)
        now = time.time()
        
        tasks._savePreviewResult(True, result, prevData.command_execution.id, now)
        
        self.assertEquals(seqId, models.PreviewData.objects.get(pk=prevData.pk).sequence_id)
        
        # update the result with some values and a higher sequence_id
        newSeqId = seqId + 1
        result.addValue(x=1, y=2, series=3, info=4)
        result.sequence_id = seqId + 1
        
        tasks._savePreviewResult(True, result, prevData.command_execution.id, now)
        newData = models.PreviewData.objects.get(pk=prevData.pk)
        self.assertEquals(newSeqId, newData.sequence_id)
        self.assertEquals(2, newData.data['values'][0]['y'])
        self.assertEquals(4, newData.data['values'][0]['info'])
        
    def test_savePreviewResult_error(self):
        
        prevData = self._createPreviewData()
        
        now = time.time()
        
        tasks._savePreviewResult(False, "some error", prevData.command_execution.id, now)
        
        newPrevData = models.PreviewData.objects.get(pk=prevData.pk)
        self.assertEquals("some error", newPrevData.data)


    def test_PreviewCommand_internal(self):
        exe = self._createCommandExecution()
        with open(os.path.join('/tmp', tasks.STDOUT_FILE), 'w') as testout:
            print >> testout, 'hello world'
        
        tasks.PreviewCommand.delay(exe.pk, '/tmp', 'echo-parser', ignoreErrors=False)
        self.assertEquals('hello world', exe.preview_data.data['values'][0]['info'].strip())
        
    def test_PreviewCommand_internal_stderrexpand(self):
        exe = self._createCommandExecution()
        with open(os.path.join('/tmp', tasks.STDERR_FILE), 'w') as testout:
            print >> testout, 'hello world2'
        
        tasks.PreviewCommand.delay(exe.pk, '/tmp', 'echo-parser %stderr', ignoreErrors=False)
        self.assertEquals('hello world2', exe.preview_data.data['values'][0]['info'].strip())
        
    def test_PreviewCommand_internal_abspathexpand(self):
        exe = self._createCommandExecution()
        with open(os.path.join('/tmp', tasks.STDOUT_FILE), 'w') as testout:
            print >> testout, 'hello world3'
        
        tasks.PreviewCommand.delay(exe.pk, '/tmp', 'echo-parser $' + tasks.STDOUT_FILE, ignoreErrors=False)
        self.assertEquals('hello world3', exe.preview_data.data['values'][0]['info'].strip())
        
    def test_PreviewCommand_internal_errors(self):
        exe = self._createCommandExecution()
        
        self.assertRaises(RuntimeError, tasks.PreviewCommand.delay, exe.pk, '/tmp', 'echo-parser too many arguments', ignoreErrors=False)
        
        
    def test_PreviewCommand_external(self):
        with open(os.path.join('/tmp', tasks.STDOUT_FILE), 'w') as testout:
            print >> testout, '[1,2,3,'
            
        with open('/tmp/anyout', 'w') as anyout:
            print >> anyout, '4,5,6]'
            
        exe = self._createCommandExecution()
        tasks.PreviewCommand.delay(exe.pk, '/tmp', 'cat %stdout $anyout', ignoreErrors=False)
        
        self.assertEquals([1, 2, 3, 4, 5, 6], exe.preview_data.data['values'])
        
    def test_PreviewCommand_failing(self):
            
        exe = self._createCommandExecution()
        self.assertRaises(OSError, tasks.PreviewCommand.delay, exe.pk, '/tmp', 'invalidcommand', ignoreErrors=False)
        
        self.assertEquals(models.PreviewData.PREVIEW_ERROR, exe.preview_data.status)
