'''

@author: eh14
'''



from django.test import TestCase
from modsched.execution.parsers import prism
import os
from modsched.execution import parsers
from modsched import execution
import jsonfield.fields
import StringIO

INPUT_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'parseinput')

class FileSizeWrapper(object):
    def __init__(self, f, maxRead):
        self._f = f
        self._maxRead = maxRead
        
    def read(self, length=None):
        if self._maxRead <= 0:
            return ""
        
        if length is None:
            toRead = self._maxRead
        else:
            toRead = min(self._maxRead, length)
            
        self._maxRead -= toRead
        
        return self._f.read(toRead)

class TestPrismParser(TestCase):
    def test_V4_2_beta1(self):
        
        ifileName = os.path.join(INPUT_DIR, 'v4.2.beta1_log.txt')
        
        parser = prism.PrismParserV4_2_beta1()
        
        result = parser.ParserResult('id')
        parser.parse(ifileName, result)
        self.assertEquals(8, len(result.values))
        
    def test_V4_2_beta1_partial(self):
        self._testPartialParse(os.path.join(INPUT_DIR, 'v4.2.beta1_log.txt'), prism.PrismParserV4_2_beta1)
        
    def _testPartialParse(self, inFileName, parserClass):
        size = 0
        maxSize = os.stat(inFileName).st_size
        increase = maxSize / 10
        successful = 0
        
        parser = parserClass()
        
        while True:
            size += increase
            with open(inFileName, 'r') as inFile:
                limitedFile = FileSizeWrapper(inFile, size)
                result = parser.ParserResult('id')
                try:
                    parser.parse(limitedFile, result)
                    successful += 1
                except parsers.LogParseException:
                    pass  # ok, that is expected
                
            # only increase until we reach the file size
            if size > maxSize:
                break
            
        self.assertTrue(successful > 0)
        
    def test_Quantile(self):
        parser = prism.PrismParserQuantile()
        
        result = parser.ParserResult('id')
        parser.parse(os.path.join(INPUT_DIR, 'v4.2.beta1_log.txt'), result)

        print "TODO: use correct log, verify output"
        
    def test_V4_0_3(self):
        parser = prism.PrismParserV4_0_3()
        
        result = parser.ParserResult('id')
        parser.parse(os.path.join(INPUT_DIR, 'v4.2.beta1_log.txt'), result)
        
        self.assertEquals(8, len(result.values))
        
        print "TODO: use correct log, verify output"
        
    def test_allParserValidateJson(self):
        # TODO: use all parsers, map them to their test outputfiles.
        # parsers = execution.PARSER_REGISTRY.values()
        
        testParsers = [(prism.PrismParserV4_2_beta1, 'v4.2.beta1_log.txt'),
                   (prism.PrismParserV4_0_3, 'v4.2.beta1_log.txt'),
                   (parsers.EchoParser, 'plaininput.txt'),
                   ] 
        
        for parserClass, inFile in testParsers:
            parser = parserClass()
            
            result = parser.ParserResult('id')
            parser.parse(os.path.join(INPUT_DIR, inFile), result)
            
            
            # try to encode and decode it with the
            # json-converts, the field's going to use!
            self.assertDictEqual(result.toDict(),
                                 jsonfield.fields.json.loads(jsonfield.fields.json.dumps(result.toDict())))
        
class TestEchoParser(TestCase):        
    def test_parse(self):
        parser = parsers.EchoParser()
        
        r = parser.ParserResult('id')
        parser.parse(StringIO.StringIO("test"), r)
        
        self.assertEquals('test', r.values[0]['info'])
        
class TestLinePreviewResult(TestCase):
    def test_toJson(self):
        result = parsers.LinePreviewResult(123)
        result.addValue(1, 2, 3, 4)
        self.assertEquals(123, result.toDict()['sequence_id'])
        self.assertEquals('line', result.toDict()['previewType'])
        self.assertEquals([{'x':1, 'y':2, 'series':3, 'info':4}], result.toDict()['values'])
