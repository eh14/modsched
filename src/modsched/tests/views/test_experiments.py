from django.test import TestCase, Client
from django.core.urlresolvers import reverse
import json
from rest_framework.test import APIClient

from modsched import models
from modsched.tests import creator

class TestMakeCommands(TestCase, creator.CreatorMixin):
    
    def setUp(self):
        self.c = APIClient()
        self.u = self._createUser()
        self.c.force_authenticate(user=self.u)
        
    def test_single(self):
        response = self.c.post(reverse('experiments_makecommands', kwargs={'experimentId':0}), {'command':'cmd', 'arguments':'arg1 arg2'})
        self.assertEquals(200, response.status_code)
        self.assertEquals(['cmd arg1 arg2'], json.loads(response.content))
        
    def test_multiple(self):
        url = reverse('experiments_makecommands', kwargs={'experimentId':0})
        response = self.c.post(url, {'command':'cmd',
                                    'arguments':'arg1 <0:1> arg2'})
        self.assertEquals(['cmd arg1 0 arg2', 'cmd arg1 1 arg2'],
                          json.loads(response.content))
        
        response = self.c.post(url, {'command':'',
                                    'arguments':'<2>'})
        self.assertEquals(set([' 0', ' 1', ' 2']), set(json.loads(response.content)))
        
        response = self.c.post(url, {'command':'',
                                    'arguments':'<1:2>'})
        self.assertEquals(set([' 1', ' 2']), set(json.loads(response.content)))
        
        response = self.c.post(url, {'command':'',
                                    'arguments':'<10:10:30>'})
        self.assertEquals(set([' 10', ' 20', ' 30']), set(json.loads(response.content)))
        
        
    def test_floats(self):
        url = reverse('experiments_makecommands', kwargs={'experimentId':0})
        response = self.c.post(url, {'command':'cmd',
                                    'arguments':'arg1 <0:1> arg2'})
        self.assertEquals(['cmd arg1 0 arg2', 'cmd arg1 1 arg2'],
                          json.loads(response.content))
        
        response = self.c.post(url, {'command':'',
                                    'arguments':'<2>'})
        self.assertEquals(set([' 0', ' 1', ' 2']), set(json.loads(response.content)))
        
        response = self.c.post(url, {'command':'',
                                    'arguments':'<1:2>'})
        self.assertEquals(set([' 1', ' 2']), set(json.loads(response.content)))
        
        response = self.c.post(url, {'command':'',
                                    'arguments':'<10:10:30>'})
        self.assertEquals(set([' 10', ' 20', ' 30']), set(json.loads(response.content)))


class TestExperimentsViewSet(TestCase, creator.CreatorMixin):
    def setUp(self):
        self.c = APIClient()
        self.u = self._createUser('test')
        self.c.force_authenticate(user=self.u)
        
    def test_list(self):
        exp = models.Experiment.objects.create(title='test', creator=self.u)
        response = self.c.get(reverse('experiments-list'))
        self.assertTrue(len(response.data) >= 1)
        allIds = [e['id'] for e in response.data]
        self.assertTrue(exp.id, allIds)
        
    def test_create(self):
        
        posted = self.c.post(reverse('experiments-list'), data={'title':'hello'},)
        self.assertEquals(201, posted.status_code)
        
        response = self.c.get(reverse('experiments-list'))
        
        titles = [d['title'] for d in response.data]
        self.assertTrue('hello' in titles)
        
class TestCommandViewSet(TestCase, creator.CreatorMixin):
    def setUp(self):
        self.c = APIClient()
        self.u = self._createUser('test')
        self.c.force_authenticate(user=self.u)
    
    def test_list_per_experiment(self):
        exp1 = self._createExperiment(creator=self.u)
        exp2 = self._createExperiment(creator=self.u)
        
        cmd1 = self._createCommand(experiment=exp1)
        cmd2 = self._createCommand(experiment=exp2)
        
        response = self.c.get(reverse('commands-list'), {'experiment_id':exp1.id})
        self.assertEquals(200, response.status_code)
        self.assertTrue(cmd1.id in [c['id'] for c in response.data])
        
        response = self.c.get(reverse('commands-list'), {'experiment_id':exp2.id})
        self.assertEquals(200, response.status_code)
        self.assertTrue(cmd2.id in [c['id'] for c in response.data])
        
        
    def test_list_per_experiment_invalid(self):
        response = self.c.get(reverse('commands-list'))
        self.assertEquals(404, response.status_code)
        
        
    def test_createCommand(self):
        exp = self._createExperiment()
        exe = self._createExecutable()
        res = self.c.post(reverse('commands-list'), data={'experiment':exp.id,
                                                     'executable':exe.path,
                                                     'arguments':['arg'],
                                               'previewer':'mypreviewer',
                                               'previewer_series':'series'})
        self.assertEquals(201, res.status_code)
        cmd = list(exp.command_set.all())[0]
        self.assertEquals('mypreviewer', cmd.previewer)
        self.assertEquals('series', cmd.previewer_series)
        
        
class TestCommandExecutionViewSet(TestCase, creator.CreatorMixin):
    
    def setUp(self):
        self.c = APIClient()
        self.u = self._createUser('test')
        self.c.force_authenticate(user=self.u)
        
    def test_list(self):
        # create a command with 4 command-executions
        cmd = self._createCommand(arguments=['<1:5>'], arguments_expand=True)
        
        r = self.c.get(reverse('executions-list'), {'experiment_id':cmd.experiment.id})
        
        self.assertEquals(200, r.status_code)
        
        self.assertEquals(5, len(r.data))

    def test_list_results(self):
        # create a command with only one execution
        cmd = self._createCommand()
        
        # retrieve the one execution
        executions = cmd.executions.all()
        self.assertEquals(1, len(executions))
        execution = list(executions)[0]
        
        # create a task state and save it into the execution
        task = self._createTaskState()
        execution.command_task = task
        execution.save()
        
        # create preview data assigned to the execution
        self._createPreviewData(command_execution=execution, data={'hello':'world'})
        
        # retrieve the serialized data and check both results are there.
        r = self.c.get(reverse('executions-list'), {'experiment_id':cmd.experiment.id})
        
        self.assertEquals(1, len(r.data))
        
        jsonExe = r.data[0]
        self.assertEquals(task.task_id, jsonExe['command_task']['task_id'])
        self.assertEquals({'hello':'world'}, jsonExe['preview_data']['data'])
        
    def test_put(self):
        cmd = self._createCommand(self._createExperiment(creator=self.u))
        exe = list(cmd.executions.all())[0]
        
        self.assertEquals('x', exe.preview_mapping['x'])
        
        r = self.c.put(reverse('executions-detail', kwargs={'pk':exe.id}), data={'preview_mapping':
                                                                                            {'x':'dubdidoo'}},
                                                                                            format='json'
                    )
        self.assertEquals(200, r.status_code)
        
        # reload
        exe = models.CommandExecution.objects.get(pk=exe.pk)
        self.assertEquals('dubdidoo', exe.preview_mapping['x'])
        
        
