from modsched import models
from modsched.services import experiment, store
from modsched.tests import creator
import os

from celery import states
from django.test import TestCase
from django.utils import timezone
from djcelery import models as djmodels


class TestExperimentService(TestCase, creator.CreatorMixin):
    
    def setUp(self):
        self.u = self._createUser("test_experiment_service")
        
#     def test_replaceExperimentCommands(self):
#         ex = self._createExperiment(creator=self.u, title="test",
#                                               command='echo',
#                                               arguments='hello world [1:3]')
#         
#         experiment.replaceExperimentCommands(ex)
#         
#         
#         self.assertEquals(3, len(ex.commandstep_set.all()))
#         
#         experiment.replaceExperimentCommands(ex)


    def test_startExperiment(self):
        """ Tests starting an experiment. """
        
        exp = self._createExperiment(creator=self.u)
        executable = self._createExecutable('echo')
        self._createCommand(experiment=exp, executable=executable,
                            arguments=['1'])
        self._createCommand(experiment=exp, executable=executable,
                            arguments=['2'])
        self._createCommand(experiment=exp, executable=executable,
                            arguments=['3'])
        
        results = experiment.startById(exp.id)
        
        self.assertEquals(3, len(results))
        
        for result in results:
            self.assertTrue(result.successful())
        
        
    def test_startExperiment_deleteold(self):
        exp = self._createExperiment(creator=self.u)
        executable = self._createExecutable('echo')
        command = self._createCommand(experiment=exp, executable=executable,
                            arguments=['1'])
        
        task = self._createTaskState(task_id='123', state=states.SUCCESS,
                                                 tstamp=timezone.now())
        
        # we know we have one execution, so
        # let's assign our artificial task to the execution and save it
        execution = list(command.executions.all())[0]
        execution.command_task = task
        execution.save()
        
        
        # try if the task is there
        self.assertEquals('123', models.CommandExecution.objects.get(pk=execution.pk).command_task.task_id)
        
        # start the experiment....
        experiment.startById(exp.id)
        
        # the task should be deleted, so let's try to access it and assert the 
        # not-found-exception
        self.assertRaises(djmodels.TaskState.DoesNotExist, lambda:models.CommandExecution.objects.get(pk=execution.pk).command_task.state)
        
        
    def test_expandInputFiles(self):
        exe = self._createCommandExecution(arguments=['$input.txt',
                                                      '$nonexisting',
                                                      'noinput'])

        inputFolder = store.getInputFolderName(exe.command.experiment_id)
        existingFile = os.path.join(inputFolder, 'input.txt')
        with open(existingFile, 'w')as out:
            print >> out, "blubb"
        
        expandedCommand = experiment.expandInputFiles(exe.command_string, exe.command.experiment_id)
        # the existing file is expanded
        self.assertTrue(existingFile in expandedCommand)
        
        # no $, not expanded
        self.assertTrue('noinput' in expandedCommand)
        
        # nonexisting file not expanded
        self.assertTrue('$nonexisting' in expandedCommand)
        
        
        
        
#     def test_start_single(self):
#         ex = self._createExperiment(creator=self.u, title='test',
#                                command="echo",
#                                arguments="hello world")
#         
#         experiment.replaceExperimentCommands(ex)
#         
#         self._createCommandTaskState(ex.commandstep_set.all()[0], 'test-taskid')
#         
#         self.assertEquals('SUCCESS', utils.waitForTask(r[0], 5))
#             
#     def test_stop_single(self):
#         ex = self._createExperiment(creator=self.u, title='test',
#                                command="sleep",
#                                arguments="10")
#         
#         experiment.replaceExperimentCommands(ex)
#         
#         r = experiment.start(ex)
#         self.assertRaises(AssertionError, utils.waitForTask, r[0], 0.5)
#         experiment.stop(ex)
#         self.assertEquals('REVOKED', utils.waitForTask(r[0], 5))
#         
#         
#     def test_start_multiple(self):
#         ex = self._createExperiment(creator=self.u, title='test',
#                                     command='echo',
#                                     arguments='hello world #[10]',)
#         
#         experiment.replaceExperimentCommands(ex)
#         jobs = experiment.start(ex)
#         self.assertEquals(11, len(jobs))
#         
#         for job in jobs:
#             self.assertEquals('SUCCESS', utils.waitForTask(job, 5))
#             
#     def test_stop_multiple(self):
#         ex = self._createExperiment(creator=self.u, title='test',
#                                command="sleep",
#                                arguments="2[5]")
#         
#         experiment.replaceExperimentCommands(ex)
#         
#         r = experiment.start(ex)
#         self.assertRaises(AssertionError, utils.waitForTask, r[0], 0.5)
#         experiment.stop(ex)
#         self.assertEquals('REVOKED', utils.waitForTask(r[0], 20))
        
