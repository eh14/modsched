'''

@author: eh14
'''

from modsched import serializers, models
from modsched.tests import creator

from django.test import TestCase
from django.utils import timezone
from djcelery import models as djmodels


class TestExperimentSerializer(TestCase, creator.CreatorMixin):
    def setUp(self):
        self.u = self._createUser('test_experiment_serializer')
        
    def test_get(self):
        
        ex = self._createExperiment(title='title', creator=self.u)
        serEx = serializers.ExperimentSerializer(ex)
        
        self.assertEquals('title', serEx.data.get('title'))
        
        
    def test_create(self):
        emptySerializer = serializers.ExperimentSerializer(data={'title':'title'})
        self.assertTrue(emptySerializer.is_valid())
        emptySerializer.save(creator=self.u)
        
    def test_update(self):
        ex = self._createExperiment(title='title', creator=self.u)
        serEx = serializers.ExperimentSerializer(ex, data={'title':'blubber'})
        self.assertTrue(serEx.is_valid())
        serEx.save()
        
        # reload, check the title really changed
        ex2 = models.Experiment.objects.get(pk=ex.pk)
        self.assertEquals('blubber', ex2.title)


class TestCommandSerializer(TestCase, creator.CreatorMixin):
    
    def setUp(self):
        self.u = self._createUser('user')
        
    def test_get(self):
        cs = self._createCommand()
        
        serCs = serializers.CommandSerializer(cs)
        self.assertEquals(cs.executable, serCs.data['executable'])
        
    def test_create(self):
        exp = self._createExperiment()
        exe = self._createExecutable()
        # create the serializer, validate and perform the creation
        emptySerializer = serializers.CommandSerializer(data={'arguments':['a', 'b'],
                                                              'previewer':'somepreviewer',
                                                              'previewer_series':'prev-series',
                                                              'experiment':exp.id,
                                                              'executable':exe.path})
        # raises an exception if invalid
        emptySerializer.is_valid(True)
        emptySerializer.save()
        
        # retrieve the object from database
        cmdObject = list(exp.command_set.all())[0]
        
        self.assertEquals(['a', 'b'], cmdObject.arguments)
        self.assertEquals('somepreviewer', cmdObject.previewer)
        self.assertEquals('prev-series', cmdObject.previewer_series)
        
    def test_update(self):
        self.skipTest('update not implemented')
        # self.skipTest("Update not implemented for this serializer")
        # create the commandstep
        cs = self._createCommand()
        
        # wrap it in a serializer, ready for update
        serCs = serializers.CommandSerializer(cs, data={'command':'mycommand22'})
        
        # validate and save
        self.assertTrue(serCs.is_valid())
        serCs.save()
        
        # reload, check the value really changed
        cs2 = models.Command.objects.get(pk=cs.pk)
        # it's read only, no update allowed via rest (yet)
        self.assertEquals('mycommand', cs2.command)

    def test_relatives(self):
        self.skipTest('unstable')
        cs = self._createCommandStep('mycommand')
        
        cs.parse_result = models.PreviewData(sequence_id=15, metaInfo={'meta':'info'},
                                          results=[1, 2, 3],
                                          runtime=123.3)
        
        cs.command_task = djmodels.TaskState.objects.update_or_create(tstamp=timezone.now())
        
        serCs = serializers.CommandSerializer(cs)
        self.assertEquals(15, serCs.data['parse_result']['sequence_id'])
        self.assertEquals([1, 2, 3], serCs.data['parse_result']['results'])
        self.assertEquals({'meta':'info'}, serCs.data['parse_result']['metaInfo'])
