'''

@author: eh14
'''
from __future__ import absolute_import

from modsched import models
import uuid

from celery import states
from django.utils import timezone
from djcelery import models as djmodels


class CreatorMixin(object):
    
    def _createExperiment(self, **kwargs):
        if 'title' not in kwargs:
            kwargs['title'] = str(uuid.uuid4())[:10]
            
        if 'creator' not in kwargs:
            kwargs['creator'] = self._createUser('testuser' + str(uuid.uuid4())[:10])
        return models.Experiment.objects.create(**kwargs)

    def _createUser(self, name=None, **kwargs):
        if name is None:
            name = 'user-' + str(uuid.uuid4())[:5]
        return models.User.objects.create(username=name, **kwargs)
        
    def _createExecutable(self, path='test-exec', arguments=[{'name':'prop1'}, {'name':'prop2'}, {'name': 'prop3'}]):
        
        executable = models.Executable.objects.create(path=path)
        
        for prop in arguments:
            prop = models.Argument.objects.create(executable=executable,
                                                  name=prop['name'], choices=prop.get('choices', []))
        return executable
    
    def _createCommand(self, experiment=None, executable=None, **kwargs):
        if experiment is None:
            experiment = self._createExperiment()
            
        if executable is None:
            executable = self._createExecutable()
            
        return models.Command.objects.create(experiment=experiment,
                                             executable=executable.path,
                                             **kwargs)
        
    def _createCommandExecution(self, *args, **kwargs):
        cmd = self._createCommand(*args, **kwargs)
        return list(cmd.executions.all())[0]
    
    
    def _createTaskState(self, **kwargs):
        kwargs.setdefault('task_id', str(uuid.uuid4()))
        kwargs.setdefault('tstamp', timezone.now())
        kwargs.setdefault('state', states.SUCCESS)
        
        return djmodels.TaskState.objects.create(**kwargs)
    
    
    def _createPreviewData(self, **kwargs):
        if 'command_execution' not in kwargs:
            kwargs['command_execution'] = self._createCommandExecution()
        
        kwargs.setdefault('sequence_id', 1)
        kwargs.setdefault('data', {'type':'line',
                                   'values':['a', 'b', 'c']})
        kwargs.setdefault('runtime', 1.00023)
        return models.PreviewData.objects.create(**kwargs)
