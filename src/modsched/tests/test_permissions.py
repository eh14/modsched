'''

@author: eh14

'''
from django.test import TestCase
from modsched.tests import creator
from rest_framework.test import APIClient
from django.core.urlresolvers import reverse

class TestPermissions(TestCase, creator.CreatorMixin):
    def setUp(self):
        self.c = APIClient()
        self.u = self._createUser()
        self.c.force_authenticate(user=self.u)
        
    def test_ExperimentViewSet(self):
        expOwned = self._createExperiment(creator=self.u)
        expOther = self._createExperiment()


        r = self.c.put(reverse('experiments-detail', kwargs={'pk':expOwned.pk}),
                       data={'title':'change'})
        
        self.assertEquals(200, r.status_code)
        
        r = self.c.put(reverse('experiments-detail', kwargs={'pk':expOther.pk}),
                       data={'title':'change'})
        self.assertEquals(403, r.status_code)
        
        
    def test_CommandViewSet(self):
        expOwned = self._createExperiment(creator=self.u)
        cmdOwned = self._createCommand(experiment=expOwned)
        cmdOther = self._createCommand()
        
        r = self.c.put(reverse('commands-detail', kwargs={'pk':cmdOwned.pk}),
               data={'arguments':['hello', 'world'],
                     'experiment':cmdOwned.experiment.pk,
                     'executable':cmdOwned.executable})
        
        self.assertEquals(200, r.status_code)
        
        r = self.c.put(reverse('commands-detail', kwargs={'pk':cmdOther.pk}),
                       data={'arguments':['hello', 'world'],
                             'experiment':cmdOther.experiment.pk,
                             'executable':cmdOther.executable})
        self.assertEquals(403, r.status_code)

    def test_CommandExecutionViewSet(self):
        expOwned = self._createExperiment(creator=self.u)
        exeOwned = self._createCommandExecution(experiment=expOwned)
        exeOther = self._createCommandExecution()
        
    
        r = self.c.put(reverse('executions-detail', kwargs={'pk':exeOwned.pk}),
                       data={'preview_mapping':{'hello':'world'}})
        self.assertEquals(200, r.status_code)
        
        r = self.c.put(reverse('executions-detail', kwargs={'pk':exeOther.pk}),
                       data={'preview_mapping':{'hello':'world'}})
        self.assertEquals(403, r.status_code)