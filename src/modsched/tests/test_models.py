from django.test import TestCase
from modsched.tests import creator
from modsched import models
from modsched.models import CommandExecution

class TestExperiment(TestCase, creator.CreatorMixin):

        
    def test_getAllExecutions(self):
        experiment = self._createExperiment()
        self._createCommand(experiment, arguments='arg1')
        self._createCommand(experiment, arguments='arg2')
        self._createCommand(experiment, arguments='arg3')
        self._createCommand(experiment, arguments='<1:4>', arguments_expand=True)
        
        self.assertEquals(7, len(list(experiment.getAllExecutions())))
        
    def test_loadAllExperimentsAndTasks(self):
        execution = self._createCommandExecution()
        task = self._createTaskState()
        
        
        execution.command_task = task
        execution.save()
        
        models.Experiment.objects.loadAllExperimentsAndTasks()
        
class TestCommandExecution(TestCase, creator.CreatorMixin):
    
    
    def test_getAllForExperiment(self):
        experiment = self._createExperiment()
        self._createCommand(experiment, arguments='<1:4>', arguments_expand=True)
        
        executions = models.CommandExecution.objects.getAllForExperiment(experiment.id)
        
        self.assertEquals(4, len(executions))
        
    def test_getMaxSequenceIdForExperiment_empty(self):
        experiment = self._createExperiment()
        self.assertIsNone(models.CommandExecution.objects.getMaxSequenceIdForExperiment(experiment.id))
        
    def test_getMaxSequenceIdForExperiment(self):
        
        
        experiment = self._createExperiment()
        self._createCommand(experiment, arguments='<1:4>', arguments_expand=True)
        
        self.assertEquals(4, models.CommandExecution.objects.getMaxSequenceIdForExperiment(experiment.id))
        
    def test_removePreviewData(self):
        
        prevData = self._createPreviewData()
        
        exe = prevData.command_execution
        
        self.assertEquals(prevData, exe.getPreviewData())
        exe.removePreviewData()
        
        exe = CommandExecution.objects.get(pk=exe.id)
        
        self.assertIsNone(exe.getPreviewData())
        
    def test_getExecutionPrefetchExperiment(self):
        exe = self._createCommandExecution()
        
        exe2 = models.CommandExecution.objects.getExecutionPrefetchExperiment(exe.pk)
        
        self.assertEquals(exe2, exe)
        
        
class TestCommand(TestCase, creator.CreatorMixin):
    
    def test_createCommand_sequence_id(self):
        exp = self._createExperiment()
        exe = self._createExecutable()
        
        cmd = models.Command.objects.create(experiment=exp, executable=exe)
        
        self.assertEquals(1, list(cmd.executions.all())[0].sequence_id)
        
        cmd2 = models.Command.objects.create(experiment=exp, executable=exe, arguments_expand=True, arguments=['<1:10>'])
        self.assertEquals(11, list(cmd2.executions.all())[-1].sequence_id)
        
    def test_update_createCommandExecutions(self):
        
        # create a command
        cmd = self._createCommand(arguments=['hello', 'world'])
        
        # there should be one execution containing hello world
        executions = list(cmd.executions.all())
        self.assertEquals(1, len(executions))
        oldSeqId = executions[0].sequence_id
        self.assertTrue('hello world' in executions[0].command_string)
        
        # modify the arguments, update
        cmd.arguments = ['newCommand']
        cmd.arguments_expand = True
        cmd.save()
        
        # there should be still one execution with the new command
        # and the same sequence_id (UPDATE)
        executions = list(cmd.executions.all())
        self.assertEquals(1, len(executions))
        self.assertTrue('newCommand' in executions[0].command_string)
        self.assertEquals(oldSeqId, executions[0].sequence_id)
        
        # create another command to block the sequence ID
        laterCommand = self._createCommand(experiment=cmd.experiment)
        
        
        # modify the arguments so that they will expand
        cmd.arguments = ['<1:3>']
        cmd.save()
        
        # now we should have 3 executions
        executions = list(cmd.executions.all())
        self.assertEquals(3, len(executions))
        
        # the sequence Ids should start *after* the new one
        self.assertEquals(list(laterCommand.executions.all())[0].sequence_id + 1, executions[0].sequence_id)
        
    def test_createCommandExecution_previewer(self):
        cmd = self._createCommand(previewer='testpreviewer')
        
        execution = list(cmd.executions.all())[0]
        self.assertEquals('testpreviewer', execution.previewer)


    def test_updateCommandExecution_previewer(self):
        cmd = self._createCommand(previewer='testpreviewer')
        
        execution = list(cmd.executions.all())[0]
        self.assertEquals('testpreviewer', execution.previewer)
        
        cmd.previewer = 'previewer2'
        
        cmd.save()
        
        # reload executions
        executions = CommandExecution.objects.filter(command_id=cmd.pk).all()
        
        self.assertEquals('previewer2', executions[0].previewer)
        
