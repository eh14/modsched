'''

@author: eh14
'''
from rest_framework import serializers
from modsched import models
from modsched import serializers as mod_serializers
from authentication.serializers import AccountSerializer


class CommandExecutionSerializer(serializers.ModelSerializer):
    command_task = mod_serializers.TaskStateSerializer()
    class Meta:
        model = models.CommandExecution

class CommandSerializer(serializers.ModelSerializer):
    executions = CommandExecutionSerializer(many=True)
    
    class Meta:
        model = models.Command
        
        
        
        
class ExperimentSerializer(serializers.ModelSerializer):
    command_set = CommandSerializer(many=True)
    creator = AccountSerializer() 
    class Meta:
        model = models.Experiment
        
        
        
        
class ExecutionStateSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    state = serializers.CharField()
    task_id = serializers.CharField()
    tstamp = serializers.DateTimeField()
    
class ExperimentStateSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    executions = ExecutionStateSerializer(many=True)
    class Meta:
        fields = ('id',
                  'executions',
                  )
        
        read_only_fields = fields

