'''

@author: eh14
'''


from authentication.serializers import AccountSerializer
from modsched.services import experiment
from modsched.services import commands
from modsched import models
from djcelery import models as djmodels

from rest_framework import serializers

class JSONSerializerField(serializers.Field):
    """
    Serializer Field for JSONField-columns in django-models.
    Needs to be declared in the serializer.
    
    from http://stackoverflow.com/a/28200902
    """
    def to_internal_value(self, data):
        return data
    def to_representation(self, value):
        return value

class ExperimentSerializer(serializers.ModelSerializer):
    creator = AccountSerializer(read_only=True, required=False)
    
    class Meta:
        model = models.Experiment
        
        fields = ('id',
                  'title',
                  'description',
                  'creator',
                  'creation_date',
                  'start_date',
                  'finish_date',
                  )
        read_only_fields = ('id',
                            'creator',
                            'creation_date',
                            'start_date',
                            'finish_date',
                            )

class TaskStateSerializer(serializers.ModelSerializer):
    tstamp = serializers.DateTimeField()
    class Meta:
        model = djmodels.TaskState
        
        fields = ('state',
                  'task_id',
                  'traceback',
                  'tstamp',
                  'runtime')
        read_only_fields = tuple(fields)

class PreviewDataSerializer(serializers.ModelSerializer):
    data = JSONSerializerField()
    
    class Meta:
        model = models.PreviewData
        
        fields = ('data',
                  'runtime',
                  'status',
                  )
    
class CommandExecutionSerializer(serializers.ModelSerializer):
    command_task = TaskStateSerializer(read_only=True, required=False)
    preview_data = PreviewDataSerializer(read_only=True, required=False)
    preview_mapping = JSONSerializerField()
    
    class Meta:
        model = models.CommandExecution
        fields = ('id',
              'command',
              'command_task',
              'command_string',
              'sequence_id',
              'previewer',
              'previewer_series',
              'preview_data',
              'preview_mapping',
              )
        
        read_only_fields = ('id',
                            'command',
                            'command_task',
                            'command_string',
                            'sequence_id',)
        
class ArgumentSerializer(serializers.ModelSerializer):
    choices = JSONSerializerField()
    class Meta:
        model = models.Argument
        fields = ('id',
                  'name',
                  'executable',
                  'isDefault',
                  'choices',)
        
        read_only_fields = ('id',
                            )

class ExecutableSerializer(serializers.ModelSerializer):
    argument_set = ArgumentSerializer(read_only=True,
                                      many=True,
                                      required=False)
    
    class Meta:
        model = models.Executable
        
        fields = ('id',
                  'path',
                  'description',
                  'argument_set',)
        
        read_only_field = ('id',)

class CommandSerializer(serializers.ModelSerializer):
    arguments = JSONSerializerField()
    executions = CommandExecutionSerializer(read_only=True, many=True, required=False)
    class Meta:
        model = models.Command
        
        fields = ('id',
                  'experiment',
                  'executable',
                  'executions',
                  'arguments_expand',
                  'arguments',
                  'previewer',
                  'previewer_series')
        
        read_only_fields = ('id',)
