'''

@author: eh14
'''
from settings import *


DATABASES = {'default': {'ENGINE': 'django.db.backends.sqlite3',
                         'NAME':'/tmp/_modsched_test.db'}}

FIXTURE_DIRS = ('modsched/fixtures',
                )

MODSCHED_PROJECTDIR = '/tmp/_test_workingdir_'

TEST_RUNNER = 'djcelery.contrib.test_runner.CeleryTestSuiteRunner'


#
# no broker etc, always execute the tasks locally
CELERY_ALWAYS_EAGER = True
CELERY_EAGER_PROPAGATES_EXCEPTIONS = True
