(function() {
    'use strict';

    angular.module('modsched.authentication', [
            'modsched.authentication.controllers',
            'modsched.authentication.services' ]);

    angular.module('modsched.authentication.controllers', []);

    angular.module('modsched.authentication.services', [ 'ngCookies' ]);
})();