(function() {
    'use strict';

    angular.module('modsched.authentication.controllers').controller(
            'LoginController', LoginController);

    LoginController.$inject = [ '$location', '$scope', 'Authentication' ];

    function LoginController($location, $scope, Authentication) {
        var vm = this;

        vm.login = function() {
            Authentication.login(vm.username, vm.password).then(null,
                    function(data, status, headers, config) {
                        $scope.error = data.data.message;
                    });
        };
    }
})();