/**
 * Authentication
 * 
 * @namespace modsched.authentication.services
 */
(function() {
    'use strict';

    angular.module('modsched.authentication.services').factory(
            'Authentication', Authentication);

    Authentication.$inject = [ '$cookies', '$http' ];

    /**
     * @namespace Authentication
     * @returns {Factory}
     */
    function Authentication($cookies, $http) {

        function login(username, password) {
            return $http.post('/api/v1/auth/login/', {
                username : username,
                password : password
            }).then(loginSuccessFn, null);

            /**
             * @name loginSuccessFn
             * @desc Set the authenticated account and redirect to index
             */
            function loginSuccessFn(data, status, headers, config) {
                Authentication.setAuthenticatedAccount(data.data);
                window.location = '/';
            }
        }

        function getAuthenticatedAccount() {
            if (!$cookies.authenticatedAccount) {
                return;
            }

            return JSON.parse($cookies.authenticatedAccount);
        }

        function isAuthenticated() {
            return !!$cookies.get('authenticatedAccount');
        }

        function setAuthenticatedAccount(account) {
            $cookies.put('authenticatedAccount', JSON.stringify(account));
        }

        function unauthenticate() {
            $cookies.remove('authenticatedAccount');
        }

        function logout() {
            return $http.post('/api/v1/auth/logout/').then(logoutSuccessFn,
                    logoutErrorFn);

            function logoutSuccessFn(data, status, headers, config) {
                Authentication.unauthenticate();
                window.location = '/login';
            }

            function logoutErrorFn(data, status, headers, config) {
                console.error('Epic failure!');
            }
        }

        /**
         * @name Authentication
         * @desc The Factory to be returned
         */
        var Authentication = {
            getAuthenticatedAccount : getAuthenticatedAccount,
            isAuthenticated : isAuthenticated,
            login : login,
            logout : logout,
            setAuthenticatedAccount : setAuthenticatedAccount,
            unauthenticate : unauthenticate
        };

        return Authentication;
    }
})();