import json

from django.contrib.auth import authenticate, login, logout

from rest_framework import permissions, status, views
from rest_framework.response import Response

from authentication.serializers import AccountSerializer

class LoginView(views.APIView):
    permission_classes = (permissions.AllowAny,)
        
    def post(self, request, format=None):
        data = json.loads(request.body)

        print data
        username = data.get('username', None)
        password = data.get('password', None)
        print username, password
        account = authenticate(username=username, password=password)

        if account is not None:
            if account.is_active:
                login(request, account)

                serialized = AccountSerializer(account)

                return Response(serialized.data)
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'This account has been disabled.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'Username/password combination invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)
            
    
            
class LogoutView(views.APIView):

    def post(self, request, format=None):
        logout(request)

        return Response({}, status=status.HTTP_204_NO_CONTENT)