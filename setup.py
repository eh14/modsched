from setuptools import setup, find_packages

setup(
    name="modsched",
    version="1.0",
    url='',
    license='',
    description="",
    author='Franz Eichhorn',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=['setuptools',
                      'fabric',
                        'django-bower',
                        'MySQL-python',
                        'djangorestframework==3.1.1',
                        'drf-nested-routers==0.9.0',  # for SimpleNestedRouter
                        'markdown',
                        'celery==3.1.18',
                        'django_compressor==1.4',
                        'django-celery==3.1.16',
                        'pytest-django==2.8.0',
                        'requests',
                        'SQLAlchemy',
                        'pyparsing==2.0.3',
                        'jsonfield==1.0.3',
                        'python-hglib',
                        'uwsgi',
                        'mercurial',
                        'pylint',
                        'pytz', # for django-timzones
                        ],
     classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
    ]
)
