# README #

This application allows to define and execute scripts in parallel providing a web 
interface to control everything.

### Setup and Dev-Deployment ###

In general, you'll need rabbitmq for the task queue and mysql plus its client-devlibs for the web application. 
You'll also probably need linux, never tested it anywhere else.

Some packages need to compile, and thus need the python-dev package for the necessary headers.

To avoid polluting your python environment, use virtualenv e.g. via pip. 

This command installs all global dependencies for the project on debian-based systems.
```
sudo apt-get install rabbitmq-server mysql-server libmysqlclient-dev python-pip python-dev
sudo pip install virtualenv
```

Nodejs's package manager is also needed to install the frontend packages. Depending on the system, the system-package to be installed is either `nodejs` or `npm`. If you get an error similar to this: */usr/bin/env: node: No such file or directory* 
follow in structions at [this issue](https://github.com/joyent/node/issues/3911) (which is basically `ln -s /usr/bin/nodejs /usr/bin/node`).


Then, clone the repo, create the virtualenv, activate it and start bootstrapping the system

```
git clone https://eh14@bitbucket.org/eh14/modsched.git
cd modsched
# create virtualenv in folder venv 
# (this is assumed by the fabfile and must be changed there if different)
virtualenv venv
# activate the virtual env
source venv/bin/activate
```

Since fabric is not virtualenv-aware it assumes your environment is stored in venv. If not, change the setting in the top of the fabfile.

Now run

```
fab bootstrap
```

to install all python dependencies using pip, bower via npm and all frontend-libraries with bower.

Now set up the database and check correct functionality with the unittests

```
fab migrate # also creates a super user. If not intended, just abort with Ctrl-C
fab check
```

Now you can start a worker and a development webserver in two different terminals:

```
fab worker
fab serve
```

Keep in mind to activate the virtualenv (if using) whenever you try to run tasks, otherwise it'll most likely complain about missing packages!



### Production-Deployment ###

This is a "normal" django application so it can be set up as any wsgi-application.
The fabric task *deploy* helps generating configuration files for uwsgi and nginx if you use that.
If you wish to go that way, just do

```
fab deploy
fab statics # to collect all statics served by nginx
``` 

optionally passing the settings port, domain, nginx_home and virtenv_home as you wish.

Restart nginx and start the uwsgi-server with 

```
sudo service nginx restart
uwsgi --ini modsched_uwsgi.ini
```

At each server start, the repository should also be synchronized with the experiments to avoid unexpected/missing folders. A shell script to be executed at startup might look like

```
#!/bin/bash

cd [project-dir]
source venv/bin/activate
fab initRepo 
uwsgi --ini modsched_uwsgi.ini
```